response.title = settings.title
response.subtitle = settings.subtitle
response.meta.author = '%(author)s <%(author_email)s>' % settings
response.meta.keywords = settings.keywords
response.meta.description = settings.description

home = [(I(_class="fa fa-home") + T('Home'), False, URL('default','index'), [])]

internal_home = home

icon_sub_menu = I(_class="fa fa-angle-double-right")
icon_sub_sub_menu = I(_class="fa fa-angle-right")

configuration = [
    (I(_class="fa fa-wrench") + T('Configuración') + I(_class="fa pull-right fa-angle-down"), False, None,
     [
        (icon_sub_menu + T('Basicos'), False, None,[
            (icon_sub_sub_menu + T('Areas'), False, URL('config', 'areas'),[]),
            (icon_sub_sub_menu + T('Nombres C. de Costos'), False, URL('config', 'nombres_centros_costos'),[]),
            (icon_sub_sub_menu + T('Centros de Costos'), False, URL('config', 'centros_costos'),[]),
            (icon_sub_sub_menu + T('Fuentes de Financiamiento'), False, URL('config', 'fuentes_financiamientos'),[]),
            (icon_sub_sub_menu + T('Metas'), False, URL('config', 'metas'),[]),
            (icon_sub_sub_menu + T('Genericas de Gastos'), False, URL('config', 'genericas_gastos'),[]),
            (icon_sub_sub_menu + T('Cadenas Funcionales'), False, URL('config', 'cadenas_funcionales'),[]),
            (icon_sub_sub_menu + T('Unidades de Medida'), False, URL('config', 'unidades_medidas'),[]),
            (icon_sub_sub_menu + T('Clasificadores de Gastos'), False, URL('config', 'clasificadores'),[]),
            (icon_sub_sub_menu + T('Ingreso Masivo de Datos Basicos'),False,URL('config','basic_massive_input'),[]),
#            (T('Productos'), False, URL('config', 'productos'),[]),
        ]),
         (icon_sub_menu + T('Productos'), False, None,[
             (icon_sub_sub_menu + T('Administrar'),False,URL('config','productos'),[]),
             (icon_sub_sub_menu + T('Ingreso Masivo de Productos'),False,URL('config','massive_input'),[]),
        #     (T('Payment Means'),False,URL('config','payment_means'),[]),
        #     (T('Payment Methods'),False,URL('config','payment_methods'),[]),
        #     (T('Payment Kinds'),False,URL('config','payment_kinds'),[]),
        #     (T('Commercial Documents'),False,URL('config','commercial_documents'),[]),
        #     (T('Banks'),False,URL('config','banks'),[]),
        #     (T('Banks Account Types'),False,URL('config','banks_account_types'),[]),
        #     (T('Services Descriptions'),False,URL('config','services_descriptions'),[]),
         ]),
         (icon_sub_menu + T('Gastos'), False, None,[
             (icon_sub_sub_menu + T('Gastos Fijos'),False,URL('config','gastos_fijos'),[]),
        #     (T('Ingreso Masivo'),False,URL('config','massive_input'),[]),
        #     (T('Payment Means'),False,URL('config','payment_means'),[]),
        #     (T('Payment Methods'),False,URL('config','payment_methods'),[]),
        #     (T('Payment Kinds'),False,URL('config','payment_kinds'),[]),
        #     (T('Commercial Documents'),False,URL('config','commercial_documents'),[]),
        #     (T('Banks'),False,URL('config','banks'),[]),
        #     (T('Banks Account Types'),False,URL('config','banks_account_types'),[]),
        #     (T('Services Descriptions'),False,URL('config','services_descriptions'),[]),
         ]),
        (icon_sub_menu + T('Usuarios'), False, None,[
            (icon_sub_sub_menu + T('Administrar'),False,URL('config','users_manage'),[]),
            (icon_sub_sub_menu + T('Grupos'),False,URL('config','users_groups'),[]),
            (icon_sub_sub_menu + T('Membresías'),False,URL('config','users_membership'),[]),
        ]),
     ]
    ),
]

configuration_aux = [
    (I(_class="fa fa-money") + T('Configuración Economía') + I(_class="fa pull-right fa-angle-down"), False, None,
     [
        (icon_sub_menu + T('Tipos de Pagos'),False,URL('configuration_aux','tipos_pagos'),[]),
        (icon_sub_menu + T('Tipo de Procesos'),False,URL('configuration_aux','tipos_procesos'),[]),
        (icon_sub_menu + T('Comprobantes'),False,URL('configuration_aux','comprobantes'),[]),
        (icon_sub_menu + T('Monedas'),False,URL('configuration_aux','monedas'),[]),
        (icon_sub_menu + T('Bancos'),False,URL('configuration_aux','bancos'),[]),
        (icon_sub_menu + T('Tipos de Cuentas'),False,URL('configuration_aux','tipos_cuentas'),[]),
        (icon_sub_menu + T('Cuentas Bancarias'),False,URL('configuration_aux','cuentas_bancarias'),[]),
        (icon_sub_menu + T('Proveedores'),False,URL('configuration_aux','proveedores'),[]),
     ]
    ),
]

logistics = [
    (I(_class="fa fa-truck") + T('Logistica') + I(_class="fa pull-right fa-angle-down"), False, None,
     [
        (icon_sub_menu + T('Proveedores'),False,URL('configuration_aux','proveedores'),[]),
     ]
    ),
]

planeamiento = [
    (I(_class="fa fa-folder-open") + SPAN(T('Gestión')) + I(_class="fa pull-right fa-angle-down"), False, None,
     [
        (icon_sub_menu + T('Perfil'),False,URL('planeamiento','perfil'),[]),
        (icon_sub_menu + T('Objetivos'),False,URL('planeamiento','objetivos'),[]),
        (icon_sub_menu + T('Objetivos por Area'),False,URL('planeamiento','asignacion_objetivos'),[]),
        (icon_sub_menu + T('Editar Actividades'),False,URL('service','procesar_actividad'),[]),
        (icon_sub_menu + T('Reportes'), False, None,[
            (icon_sub_sub_menu + T('Presupuesto General'),False,URL('service','presupuestos'),[]),
            (icon_sub_sub_menu + T('Gastos Fijos'),False,URL('service','gastos_fijos'),[]),
            (icon_sub_sub_menu + T('Reporte Centro Costo y Actividad'),False,URL('service','reporte_especifico'),[]),
            (icon_sub_sub_menu + T('Reporte Especificas Gasto'),False,URL('service','reporte_especificas_gastos'),[]),
            (icon_sub_sub_menu + T('Reporte Diferencial'),False,URL('service','reporte_diferencial'),[]),
            (icon_sub_sub_menu + T('Reporte por Metas Presupuestales'),False,URL('reportes','metas_presupuestales'),[]),
            (icon_sub_sub_menu + T('Reporte por Metas, Actividades y Clasificadores'),False,URL('reportes','metas_actividades_clasificadores'),[]),
            #(T('Objetivos por Area'),False,URL('service','asignacion_objetivos'),[]),
            (icon_sub_sub_menu + T('Matriz 01'),False,URL('reportes','matriz_01'),[]),
            (icon_sub_sub_menu + T('Matriz 02'),False,URL('reportes','matriz_02'),[]),
            (icon_sub_sub_menu + T('Matriz 03'),False,URL('reportes','matriz_03'),[]),
            (icon_sub_sub_menu + T('Control de Errores'),False,URL('reportes','control_errores'),[]),
        ]),
     ]
    ),
]

services = [
    (I(_class="fa fa-laptop") + T('Operaciones') + I(_class="fa pull-right fa-angle-down"), False, None,
     [
        (icon_sub_menu + T('Relación de Personal Asignado'), False, URL('service', 'personal'),[]),
        (icon_sub_menu + T('Análisis FODA'), False, URL('service', 'foda'),[]),
        (icon_sub_menu + T('Planeamiento'), False, None,[
            #(T('Objetivos por Area'),False,URL('service','asignacion_objetivos'),[]),
            (icon_sub_sub_menu + T('Presupuestos por Actividades'),False,URL('service','actividades_presupuestos'),[]),
            (icon_sub_sub_menu + T('Actividades por Objetivos'),False,URL('service','actividades'),[]),
        ]),
     ]
    ),
]

operaciones = [
    (I(_class="fa fa-pencil-square-o") + T('Presupuesto') + I(_class="fa pull-right fa-angle-down"), False, None,
     [
        (icon_sub_menu + T('Certificiones'), False, None,[
            #(T('Objetivos por Area'),False,URL('service','asignacion_objetivos'),[]),
            (icon_sub_sub_menu + T('Certificar'),False,URL('operaciones','certificaciones'),[]),
            (icon_sub_sub_menu + T('Consultas'),False,URL('operaciones','consultas_certificaciones'),[])
        ]),
        (icon_sub_menu + T('Compromisos'), False, None,[
            #(T('Objetivos por Area'),False,URL('service','asignacion_objetivos'),[]),
            (icon_sub_sub_menu + T('Comprometer'),False,URL('operaciones','compromisos'),[]),
            (icon_sub_sub_menu + T('Consultas'),False,URL('operaciones','consultas_compromisos'),[])
        ]),
     ]
    ),
]

economia = [
    (I(_class="fa fa-money") + T('Economia') + I(_class="fa pull-right fa-angle-down"), False, None,
     [
        (icon_sub_menu + T('Devengues'), False, None,[
            #(T('Objetivos por Area'),False,URL('service','asignacion_objetivos'),[]),
            (icon_sub_sub_menu + T('Devengar'),False,URL('economia','devengues'),[]),
            (icon_sub_sub_menu + T('Consultas'),False,URL('economia','consultas_devengues'),[])
        ]),
        (icon_sub_menu + T('Pagados'), False, None,[
            #(T('Objetivos por Area'),False,URL('service','asignacion_objetivos'),[]),
            (icon_sub_sub_menu + T('Pagar'),False,URL('economia','pagados'),[]),
            (icon_sub_sub_menu + T('Consultas'),False,URL('economia','consultas_pagados'),[])
        ]),
     ]
    ),
]


response.menu = home
#response.menu += services
if auth.user:
    user = auth.user.id
    if auth.has_membership(user_id=user, role='root'):
        response.menu += configuration
        response.menu += configuration_aux
        response.menu += logistics
        response.menu += planeamiento
        response.menu += services
        response.menu += operaciones
        response.menu += economia
    elif auth.has_membership(user_id=user, role='logistic'):
        response.menu += logistics
    elif auth.has_membership(user_id=user, role='user'):
        response.menu += planeamiento
        response.menu += services
        response.menu += operaciones
    elif auth.has_membership(user_id=user, role='economy'):
        response.menu += services
        response.menu += economia
    elif auth.has_membership(user_id=user, role='review'):
        response.menu += services
    else:
        #response.menu += configuration
        response.menu += services
