from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = 'Planeamiento'
settings.subtitle = 'SALUDPOL'
settings.author = 'Alfonso de la Guarda Reyes'
settings.author_email = 'alfonsodg@gmail.com'
settings.keywords = ''
settings.description = ''
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = '681f3cda-df53-4617-8c06-f72e3799f379'
settings.email_server = 'localhost'
settings.email_sender = 'you@example.com'
settings.email_login = ''
settings.login_method = 'local'
settings.login_config = ''
settings.plugins = []

try:
    UPLOAD_FOLDER = request.wsgi.environ['REQUEST_URI'].split('/')[1]
except:
    UPLOAD_FOLDER = ''
UPLOAD_PATH = 'applications/%s/uploads' % UPLOAD_FOLDER
EXCEL_FILE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
OLD_EXCEL_FILE = 'application/vnd.ms-excel'
CSV_FILE = 'text/csv'
