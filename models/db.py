# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    db = DAL("postgres://saludpol:y2KSalud14@127.0.0.1:5432/lantree")
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    ## from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'
## (optional) static assets folder versioning
# response.static_version = '0.0.0'
#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
import datetime

auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()
now = datetime.datetime.now()
## create all tables needed by auth if not custom tables
#auth.define_tables(username=False, signature=False)

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'you@gmail.com'
mail.settings.login = 'username:password'

## configure auth policy
auth.settings.actions_disabled.append('register')
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True
auth.settings.create_user_groups = None

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

mail.settings.server = settings.email_server
mail.settings.sender = settings.email_sender
mail.settings.login = settings.email_login

tipos_productos = {
    0: T('Bien'),
    1: T('Servicio'),
    2: T('Proyecto')
}

status_options = {
    0: T('Activo'),
    1: T('Inactivo')
}

db.define_table('tipos_procesos',
                Field('abreviatura', 'string', label=T('Abreviatura')),
                Field('nombre', 'string', label=T('Nombre')),
                format='%(abreviatura)s - %(nombre)s')

db.define_table('tipos_pagos',
                Field('nombre', 'string', label=T('Nombre')),
                format='%(nombre)s')

db.define_table('comprobantes',
                Field('nombre', 'string', label=T('Nombre')),
                Field('serial', 'string', label=T('Serial')),
                Field('correlativo', 'integer', label=T('Correlativo')),
                Field('electronico', 'boolean', label=T('Electronico')),
                format='%(nombre)s')

db.define_table('monedas',
                Field('nombre', label=T('Nombre')),
                Field('simbolo', label=T('Simbolo')),
                format='%(nombre)s')

db.define_table('bancos',
                Field('nombre', 'string', label=T('Nombre')),
                format='%(nombre)s')

db.define_table('bancos_tipos_cuentas',
                Field('nombre', 'string', label=T('Nombre')),
                Field('detraccion', 'boolean', label=T('Detraccion')),
                format='%(nombre)s')

db.define_table('cuentas_bancarias',
                Field('nombre', 'string', label=T('Nombre')),
                Field('banco', 'reference bancos', label=T('Banco')),
                Field('tipo_cuenta', 'reference bancos_tipos_cuentas', label=T('Tipo Cuenta')),
                Field('numero', 'string', label=T('Numero')),
                Field('cci', 'string', label=T('CCI')),
                format='%(nombre)s - %(numero)s')

db.define_table('cuentas_saldos',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('fecha', 'date', label=T('Fecha'),
                      default=now),
                Field('cuenta', 'reference cuentas_bancarias', label=T('Cuenta')),
                Field('monto', 'float', label=T('Monto')),
                format='%(cuenta)s')

db.define_table('cadenas_funcionales',
                Field('codigo', 'string', label=T('Codigo')),
                Field('nombre', 'string', label=T('Cadena')),
#                Field('descripcion', 'string', label=T('Nombre')),
                format='%(codigo)s - %(nombre)s')

db.define_table('metas',
                Field('codigo', 'string', label=T('Meta')),
                Field('nombre', 'string', label=T('Nombre de Meta')),
                Field('cadena', 'reference cadenas_funcionales', label=T('Cadena Funcional')),
                #Field('nombre', 'string', label=T('Nombre')),
#                Field('fuente_financiamiento', 'string', label=T('Fuente de Financiamiento')),
                format='%(codigo)s - %(nombre)s')

db.define_table('fuentes_financiamientos',
                Field('codigo', 'string', label=T('Fuente de Financiamiento')),
                Field('nombre', 'string', label=T('Descripcion')),
                format='%(codigo)s - %(nombre)s')

db.define_table('genericas_gastos',
                Field('codigo', 'string', label=T('Generica de Gasto')),
                Field('nombre', 'string', label=T('Gasto Presupuestal')),
                Field('descripcion', 'string', label=T('Descripcion')),
                format='%(codigo)s - %(nombre)s')

db.define_table('clasificadores',
                Field('codigo', 'string', label=T('Especifica de Gasto')),
                Field('nombre', 'string', label=T('Cadena')),
#                Field('descripcion', 'string', label=T('Nombre')),
                format='%(codigo)s - %(nombre)s')

db.define_table('areas',
                Field('nombre', 'string', label=T('Oficina/Gerencia')),
#                Field('meta', 'reference metas', label=T('Meta')),
                Field('responsable', 'string', label=T('Responsable')),
#                Field('fuente_financiamiento', 'reference fuentes_financiamientos', label=T('Fuente de Financiamiento')),
#                Field('email', 'string', label=T('E-mail')),
                format='%(nombre)s')

db.define_table('denominacion_centros_costos',
                Field('nombre', 'string', label=T('Centro de Costo')),
                format='%(nombre)s')


db.define_table('centros_costos',
                Field('area', 'reference areas', label=T('Oficina/Gerencia')),
                Field('denominacion', 'reference denominacion_centros_costos', label=T('Denominacion')),
                Field('nombre', 'string', label=T('Centro de Costo')),
                Field('meta', 'reference metas', label=T('Meta')),
#                Field('meta', 'reference metas', label=T('Meta')),
                Field('responsable', 'string', label=T('Responsable')),
                Field('presupuesto', 'float', label=T('Presupuesto Institucional Apertura'), default=0),
#                Field('fuente_financiamiento', 'reference fuentes_financiamientos', label=T('Fuente de Financiamiento')),
#                Field('email', 'string', label=T('E-mail')),
#                format='%(area)s - %(nombre)s')
                format=lambda row: '%s - %s - Meta: %s' % (
                    db.areas(db.areas.id==row.area)['nombre'], row.nombre, db.metas(db.metas.id==row.meta)['codigo'])
                )
#                )


auth.settings.extra_fields['auth_user'] = [
    #Field('phone', label=T('Phone')),
    Field('area', db.areas, label=T('Area'),
          requires=IS_EMPTY_OR(IS_IN_DB(db, db.areas, '%(nombre)s'))),
]

auth.define_tables(username=False, signature=False)


db.define_table('proveedores',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('nombre', 'string', label=T('Nombre Comercial')),
                Field('nombre_legal', 'string', label=T('Nombre Legal')),
                Field('ruc', 'string', label=T('RUC')),
#                Field('activity_start', 'date', label=T('Activity Start')),
                Field('web_url', 'string', label=T('Web URL')),
#                Field('legal_representative', 'string', label=T('Legal Representative')),
                Field('ficha_electronica', 'string', label=T('Ficha Electronica')),
#                Field('currency', 'reference currencies', label=T('Currency')),
                Field('estado', 'integer', label=T('Estado'),
                      default=0, requires=IS_IN_SET(status_options)),
                format='%(ruc)s -%(nombre)s')

db.define_table('proveedores_direcciones',
                Field('register_time', 'datetime', label=T('Register Time'),
                      default=now, writable=False),
                Field('proveedor', 'reference proveedores', label=T('Proveedor')),
                Field('nombre', 'string', label=T('Nombre')),
                Field('direccion', 'string', label=T('Direccion')),
                Field('referencia', 'string', label=T('Referencia')),
#                Field('tax_address', 'boolean', label=T('Tax Address')),
                Field('telefono_principal', 'string', label=T('Telefono Central')),
#                Field('priority', 'integer', default=1, label=T('Position')),
                format='%(nombre)s')

db.define_table('proveedores_contactos',
                Field('register_time', 'datetime', label=T('Register Time'),
                      default=now, writable=False),
                Field('proveedor', 'reference proveedores', label=T('Proveedor')),
                Field('cargo', 'string', label=T('Cargo')),
                Field('dni', 'string', label=T('DNI')),
                Field('nombre', 'string', label=T('Nombre Completo')),
#                Field('phone', 'string', label=T('Phone')),
                Field('email', 'string', label=T('Email')),
                Field('skype', 'string', label=T('Skype')),
                Field('facebook', 'string', label=T('Facebook')),
                Field('twitter', 'string', label=T('Twitter')),
                Field('linkedin', 'string', label=T('Linkedin')),
                Field('annotation', 'text', label=T('Annotation')),
                Field('main', 'boolean', label=T('Representante Legal')),
                Field('notificacion', 'boolean', label=T('Notificación')),
                format='%(nombre)s')

db.define_table('proveedores_contactos_telefonos',
                Field('register_time', 'datetime', label=T('Register Time'),
                      default=now, writable=False),
                Field('proveedor_contacto', 'reference proveedores_contactos',
                      label=T('Contacto de Proveedor')),
                Field('nombre', 'string', label=T('Nombre')),
                Field('telefono', 'string', label=T('Telefono')),
                Field('extension', 'string', label=T('Extension')),
                Field('main', 'boolean', label=T('Principal')),
                format='%(nombre)s')

db.define_table('proveedores_cuentas',
                Field('register_time', 'datetime', label=T('Register Time'),
                      default=now, writable=False),
                Field('proveedor', 'reference proveedores', label=T('Proveedor')),
                Field('nombre', 'string', label=T('Nombre')),
                Field('banco', 'reference bancos', label=T('Banco')),
                Field('tipo_cuenta', 'reference bancos_tipos_cuentas', label=T('Tipo Cuenta')),
                Field('numero', 'string', label=T('Numero')),
                Field('cci', 'string', label=T('CCI')),
                Field('prioridad', 'integer', label=T('Prioridad')),
                format='%(nombre)s')

db.define_table('personal',
                Field('area', 'reference areas', label=T('Area')),
                Field('dni', 'string', label=T('DNI')),
                Field('nombres', 'string', label=T('Nombres')),
                Field('apellidos', 'string', label=T('Apellidos')),
                Field('profesion', 'string', label=T('Profesion')),
                Field('cargo', 'string', label=T('Cargo')),
                format='%(dni)s - %(nombres)s %(apellidos)s')

db.define_table('unidades_medidas',
                Field('nombre', 'string', label=T('Unidad de Medida')),
                format='%(nombre)s')

db.define_table('productos',
                Field('tipo', 'integer', label=T('Tipo'), default=0,
                      requires=IS_IN_SET(tipos_productos)),
                Field('codigo', 'string', label=T('Codigo')),
                Field('descripcion', 'string', label=T('Descripción')),
                Field('unidad_medida', db.unidades_medidas, label=T('Unidad Medida')),
                Field('costo', 'float', label=T('Costo')),
                Field('clasificador', 'reference clasificadores', label=T('Clasificador')),
                format='%(descripcion)s - %(unidad_medida)s')

db.define_table('estructuras_funcionales',
                Field('categoria', 'string', label=T('Categoria')),
                Field('meta', 'reference metas', label=T('Meta')),
                Field('cadena_funcional', 'reference cadenas_funcionales', label=T('Area')),
                Field('area', 'reference areas', label=T('Area')),
                Field('centro_costo', 'reference centros_costos', label=T('Centro Costo')),
                format='%(id)s')

db.define_table('estructura_funcional_financimiento',
                Field('estructura_funcional', 'reference estructuras_funcionales', label=T('Estructura Funcional')),
                Field('fuente_financiamiento', 'reference fuentes_financiamientos', label=T('Fuente de Financiamiento')),
                format='%(id)s')


db.define_table('fortalezas',
                Field('area', 'reference areas', label=T('Area')),
                Field('descripcion', 'text', label=T('Descripcion')),
                format='%(id)s')

db.define_table('oportunidades',
                Field('area', 'reference areas', label=T('Area')),
                Field('descripcion', 'text', label=T('Descripcion')),
                format='%(id)s')

db.define_table('debilidades',
                Field('area', 'reference areas', label=T('Area')),
                Field('descripcion', 'text', label=T('Descripcion')),
                format='%(id)s')

db.define_table('amenazas',
                Field('area', 'reference areas', label=T('Area')),
                Field('descripcion', 'text', label=T('Descripcion')),
                format='%(id)s')

db.define_table('planes',
                Field('tiempo', 'integer', label=T('Año')),
                #Field('nombre', 'string', label=T('Nombre')),
                Field('mision', 'text', label=T('Mision')),
                Field('vision', 'text', label=T('Vision')),
                format='%(mision)s')

db.define_table('objetivos_estrategicos',
                Field('plan_id', 'reference planes', label=T('Plan')),
                Field('objetivo', 'string', label=T('Objetivo')),
                format='%(objetivo)s')

db.define_table('objetivos_generales',
                Field('plan_id', 'reference planes', label=T('Plan')),
                Field('objetivo', 'string', label=T('Objetivo')),
                format='%(objetivo)s')

db.define_table('objetivos_especificos',
                Field('objetivo_general', 'reference objetivos_generales', label=T('Objetivo General')),
                Field('objetivo', 'string', label=T('Objetivo')),
                format='%(objetivo)s')

db.define_table('discusiones_generales',
                Field('objetivo_general', 'reference objetivos_generales', label=T('Objetivo General')),
                Field('usuario', db.auth_user, label=T('Usuario')),
                Field('opinion', 'text', label=T('Opinion')),
                format='%(id)s')

db.define_table('discusiones_especificos',
                Field('objetivo_especifico', 'reference objetivos_especificos', label=T('Objetivo Especifico')),
                Field('usuario', db.auth_user, label=T('Usuario')),
                Field('opinion', 'text', label=T('Opinion')),
                format='%(id)s')

db.define_table('asignacion_objetivos',
                Field('area', 'reference areas', label=T('Area')),
                Field('centro_costo', 'reference centros_costos', label=T('Centro Costo')),
                Field('objetivo_general', 'reference objetivos_generales', label=T('Objetivo General')),
                Field('objetivo_especifico', 'reference objetivos_especificos', label=T('Objetivo Especifico')),
#                Field('presupuesto', 'float', label=T('Presupuesto Apertura'), default=0),
                format=lambda row: '%s - %s' % (
                    db.objetivos_generales(db.objetivos_generales.id==row.objetivo_general)['objetivo'],
                    db.objetivos_especificos(db.objetivos_especificos.id==row.objetivo_especifico)['objetivo']))
#                format='%(objetivo_general)s - %(objetivo_especifico)s')


db.define_table('actividades',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('area', 'reference areas', label=T('Area')),
                Field('centro_costo', 'reference centros_costos', label=T('Centro de Costo')),
                Field('objetivo_referencia', 'reference asignacion_objetivos', label=T('Objetivo Especifico')),
                #Field('nombre', 'string', label=T('Nombre')),
                Field('actividad', 'string', label=T('Actividad')),
                Field('unidad_medida', db.unidades_medidas, label=T('Unidad Medida')),
                Field('meta', 'string', label=T('Meta Fisica'), comment=T('')),
                Field('mes_1', 'integer', label=T('Ene'), default=0),
                Field('mes_2', 'integer', label=T('Feb'), default=0),
                Field('mes_3', 'integer', label=T('Mar'), default=0),
                Field('mes_4', 'integer', label=T('Ab'), default=0),
                Field('mes_5', 'integer', label=T('May'), default=0),
                Field('mes_6', 'integer', label=T('Jun'), default=0),
                Field('mes_7', 'integer', label=T('Jul'), default=0),
                Field('mes_8', 'integer', label=T('Ago'), default=0),
                Field('mes_9', 'integer', label=T('Set'), default=0),
                Field('mes_10', 'integer', label=T('Oct'), default=0),
                Field('mes_11', 'integer', label=T('Nov'), default=0),
                Field('mes_12', 'integer', label=T('Dic'), default=0),
                format='%(actividad)s')


db.define_table('gastos_fijos',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('centro_costo', 'reference centros_costos', label=T('Centro Costo')),
                Field('objetivo_referencia', 'reference asignacion_objetivos', label=T('Objetivo Especifico')),
                Field('tipo', 'integer', label=T('Tipo'), default=0,
                      requires=IS_IN_SET(tipos_productos)),
                Field('actividad', 'reference actividades', label=T('Actividad')),
                Field('producto', 'reference productos', label=T('Descripción de Bienes o Servicios')),
#                Field('descripcion', 'string', label=T('Otros')),
#                Field('codigo', 'string', label=T('Codigo')),
#                Field('descripcion', 'string', label=T('Descripción')),
                Field('unidad_medida', db.unidades_medidas, label=T('Unidad Medida')),
                Field('cantidad', 'float', label=T('Meta Fisica'), default=0),
                Field('mes_1', 'float', label=T('Enero'), default=0),
                Field('mes_2', 'float', label=T('Febrero'), default=0),
                Field('mes_3', 'float', label=T('Marzo'), default=0),
                Field('mes_4', 'float', label=T('Abril'), default=0),
                Field('mes_5', 'float', label=T('Mayo'), default=0),
                Field('mes_6', 'float', label=T('Junio'), default=0),
                Field('mes_7', 'float', label=T('Julio'), default=0),
                Field('mes_8', 'float', label=T('Agosto'), default=0),
                Field('mes_9', 'float', label=T('Setiembre'), default=0),
                Field('mes_10', 'float', label=T('Octubre'), default=0),
                Field('mes_11', 'float', label=T('Noviembre'), default=0),
                Field('mes_12', 'float', label=T('Diciembre'), default=0),
                Field('monto', 'float', label=T('Monto'), default=0, writable=False),
                Field('clasificador', 'reference clasificadores', label=T('Clasificador')),
                format='%(descripcion)s - %(unidad_medida)s')


db.define_table('indicadores',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('actividad', 'reference actividades', label=T('Actividad')),
                Field('denominacion', 'string', label=T('Denominacion'), comment=T('Ej: Porcentaje de Trabajadores Capacitados - Aprobados')),
                Field('numerador', 'string', label=T('Numerador'), comment=T('Ej: Número de Trabajadores Capacitados y Aprobados en el Examen de Evaluación')),
                Field('denominador', 'string', label=T('Denominador'), comment=T('Ej: Total de Trabajadores que Recibieron Capacitación')),
                Field('linea_base', 'string', label=T('Linea Base'), comment=T('Observación: No llenar')),
                Field('valor_esperado', 'string', label=T('Valor Esperado'), comment=T('Ej: 85%')),
                Field('fuente', 'string', label=T('Fuente'), comment=T('Ej: Informe de Evaluación de PDP, Exámenes')),
                Field('periodicidad', 'string', label=T('Periodicidad'), comment=T('Ej: Semestral - Anual')),
                format='%(id)s')

db.define_table('presupuestos',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('actividad', 'reference actividades', label=T('Actividad')),
                Field('producto', 'reference productos', label=T('Descripción de Bienes o Servicios')),
                Field('descripcion', 'string', label=T('Otros')),
                Field('mes_1', 'integer', label=T('Ene'), default=0),
                Field('mes_2', 'integer', label=T('Feb'), default=0),
                Field('mes_3', 'integer', label=T('Mar'), default=0),
                Field('mes_4', 'integer', label=T('Ab'), default=0),
                Field('mes_5', 'integer', label=T('May'), default=0),
                Field('mes_6', 'integer', label=T('Jun'), default=0),
                Field('mes_7', 'integer', label=T('Jul'), default=0),
                Field('mes_8', 'integer', label=T('Ago'), default=0),
                Field('mes_9', 'integer', label=T('Set'), default=0),
                Field('mes_10', 'integer', label=T('Oct'), default=0),
                Field('mes_11', 'integer', label=T('Nov'), default=0),
                Field('mes_12', 'integer', label=T('Dic'), default=0),
                Field('costo_unitario', 'float', label=T('Costo Unitario'), default=0),
                Field('costo_total', 'float', label=T('Costo Total'), default=0),
                format='%(id)s')

db.define_table('certificaciones',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('usuario', db.auth_user, label=T('Usuario')),
                Field('centro_costo', 'reference denominacion_centros_costos', label=T('Centro Costo')),
                Field('actividad', 'reference actividades', label=T('Actividad')),
                Field('requisicion', 'string', label=T('Requisicion')),
                Field('tipo_proceso', 'reference tipos_procesos', label=T('Tipo Proceso')),
                Field('numero_proceso', 'string', label=T('No. Proceso')),
                Field('observaciones', 'string', label=T('Detalle')),
                Field('fuente_financiamiento', 'reference fuentes_financiamientos', label=T('Fuente de Financiamiento')),
                Field('status', 'integer', label=T('Estado'), default=0,
                                            requires=IS_IN_SET(status_options)),
                format='%(id)s')

db.define_table('certificaciones_detalles',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('usuario', db.auth_user, label=T('Usuario')),
                Field('certificacion', 'reference certificaciones', label=T('Certificacion')),
                Field('especifica_gasto', 'reference clasificadores', label=T('Especifica de Gasto')),
                Field('meta_presupuestal', 'reference metas', label=T('Meta Presupuestal')),
                Field('producto', 'reference productos', label=T('Descripción de Bienes o Servicios'),
                      requires=IS_NOT_EMPTY()),
                Field('observaciones', 'string', label=T('Detalle')),
                #Field('moneda', 'reference monedas', label=T('Moneda')),
                Field('importe', 'string', label=T('Importe Referencial')),
                format='%(certificacion)s - %(id)s')

db.define_table('compromisos',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('usuario', db.auth_user, label=T('Usuario')),
                Field('certificacion', 'reference certificaciones', label=T('Certificacion')),
                Field('proveedor', 'reference proveedores', label=T('Proveedor')),
                Field('orden_compra', 'string', label=T('Orden de Compra/Servicio')),
                Field('contrato', 'string', label=T('Contrato')),
                Field('observaciones', 'string', label=T('Detalle')),
                Field('status', 'integer', label=T('Estado'), default=0,
                      requires=IS_IN_SET(status_options)),
                format='%(id)s')

db.define_table('compromisos_detalles',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('usuario', db.auth_user, label=T('Usuario')),
                Field('compromiso', 'reference compromisos', label=T('Compromiso')),
                Field('certificacion_detalle', 'reference certificaciones_detalles', label=T('Detalle Certificacion')),
                Field('observaciones', 'string', label=T('Observaciones')),
                Field('ano', 'integer', label=T('Año'), default=0),
                Field('mes', 'integer', label=T('Mes'), default=0),
                Field('cantidad', 'float', label=T('Cantidad'), default=0),
                Field('monto', 'float', label=T('Monto'), default=0),
                Field('status', 'integer', label=T('Estado'), default=0,
                       requires=IS_IN_SET(status_options)),
                #format='%(id)s')
                format='%(compromiso)s - %(id)s')

db.define_table('devengues',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('usuario', db.auth_user, label=T('Usuario')),
                Field('compromiso', 'reference compromisos', label=T('Compromiso')),
                Field('comprobante_pago', 'reference comprobantes', label=T('Tipo de Comprobante')),
                Field('numero_comprobante', 'string', label=T('Numero Comprobante')),
                Field('fecha_comprobante', 'date', label=T('Fecha Comprobante')),
                Field('monto', 'float', label=T('Monto'), default=0),
                Field('status', 'integer', label=T('Estado'), default=0,
                      requires=IS_IN_SET(status_options)),
                format='%(compromiso)s - %(id)s')

db.define_table('devengues_detalles',
                Field('devengue', 'reference devengues', label=T('Devengue')),
                Field('compromiso_detalle', 'reference compromisos_detalles', label=T('Detalle de Compromiso')),
                format='%(devengue)s - %(id)s')

db.define_table('pagados',
                Field('register_time', 'datetime', label=T('Register'),
                      default=now, writable=False),
                Field('usuario', db.auth_user, label=T('Usuario')),
                Field('compromiso', 'reference compromisos', label=T('Compromiso')),
                Field('devengue', 'reference devengues', label=T('Devengue')),
                Field('tipo_pago', 'reference tipos_pagos', label=T('Tipo Pago')),
                Field('cuenta_origen', 'reference cuentas_bancarias', label=T('Cuenta Origen')),
                Field('documento_generado', 'string', label=T('Documento Generado')),
                Field('cuenta_destino', 'reference proveedores_cuentas', label=T('Cuenta Destino')),
                Field('observaciones', 'string', label=T('Observaciones')),
                Field('monto', 'float', label=T('Monto'), default=0),
                Field('status', 'integer', label=T('Estado'), default=0,
                      requires=IS_IN_SET(status_options)),
                format='%(devengue)s - %(id)s')


def insertar_costos(vals, id):
    producto = vals['producto']
    if producto is None:
        return
    data_producto = db.productos(db.productos.id==producto)
    costo = data_producto['costo']
    tipo = data_producto['tipo']
    cantidad = 0
    if tipo == 0:
        for mes in range(1,13):
            dscp_mes = 'mes_%s' % mes
            cantidad += int(vals[dscp_mes])
    costo_total = cantidad * costo
    elems = {'costo_unitario':costo, 'costo_total':costo_total}
    if tipo == 0:
        status = db(db.presupuestos.id==id).update(**elems)
        #print status
        db.commit()

def actualizar_costos(vals, id):
    if 'producto' not in id:
        return
    element_id = vals.select().first().id
    producto = id['producto']
    if producto is None:
        return
    data_producto = db.productos(db.productos.id==producto)
    costo = data_producto['costo']
    tipo = data_producto['tipo']
    cantidad = 0
    if tipo == 0:
        for mes in range(1,13):
            dscp_mes = 'mes_%s' % mes
            cantidad += int(id[dscp_mes])
    costo_total = cantidad * costo
    elems = {'costo_unitario':costo, 'costo_total':costo_total}
    if tipo == 0:
        status = db(db.presupuestos.id==element_id).update(**elems)
        #print status
        db.commit()

def insertar_totales(vals, id):
    monto = 0
    for mes in range(1,13):
        dscp_mes = 'mes_%s' % mes
        monto += vals[dscp_mes]
    monto = round(monto, 2)
    status = db(db.gastos_fijos.id==id).update(monto=monto)
    #print status
    db.commit()

def actualizar_totales(vals, id):
    if 'mes_1' not in id:
        return
    element_id = vals.select().first().id
    monto = 0
    for mes in range(1,13):
        dscp_mes = 'mes_%s' % mes
        monto += id[dscp_mes]
    status = db(db.gastos_fijos.id==element_id).update(monto=monto)
    #print status
    db.commit()


def name_data(table, id_val, field='nombre'):
    """
    Content Data Representation
    """
    try:
        value_data = table(id_val)[field]
    except:
        value_data = ''
    data = '%s' % value_data
    data = data.decode('utf8')
    return data

def multi_name(value, row):
    """
    Content Data Representation
    """
    area = db.areas(db.areas.id==row.area)['nombre']
    data = '%s - %s' % (area, row.nombre)
    return data

#centros_costos_dict = {''}

db.productos.tipo.represent = lambda  value, row: None if value is None else tipos_productos[value]
db.gastos_fijos.producto.widget = lambda field,value: autocomplete_widget(field,value,base='productos',
                content='descripcion',format='descripcion,unidad_medida',aux_base='unidad_medida:unidades_medidas:nombre')
#db.centros_costos.id.represent = lambda value, row: None if value is None else multi_name(value, row)
db.presupuestos.producto.widget = lambda field,value: autocomplete_widget(field,value,base='productos',
                content='descripcion',format='descripcion,unidad_medida',aux_base='unidad_medida:unidades_medidas:nombre')
db.certificaciones.actividad.widget = lambda field,value: autocomplete_widget(field,value,base='actividades',
                content='actividad',format='actividad,unidad_medida',aux_base='unidad_medida:unidades_medidas:nombre')
db.certificaciones_detalles.producto.widget = lambda field,value: autocomplete_widget(field,value,base='productos',
                content='descripcion',format='descripcion,unidad_medida',aux_base='unidad_medida:unidades_medidas:nombre')
db.presupuestos._after_insert.append(lambda values, id: insertar_costos(values, id))
db.presupuestos._after_update.append(lambda values, id: actualizar_costos(values, id))
db.gastos_fijos._after_insert.append(lambda values, id: insertar_totales(values, id))
db.gastos_fijos._after_update.append(lambda values, id: actualizar_totales(values, id))