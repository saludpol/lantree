# -*- coding: utf-8 -*-
import uuid
from gluon.contrib.pyfpdf import FPDF, HTMLMixin
import qrcode
import hashlib

def autocomplete_widget(field, value, base=False,
        content=False, format=False, aux_base=False):
    if format is False:
        format = content
    if aux_base is False:
        aux_base = ''
    id = "autocomplete-" + str(uuid.uuid4())
    callback_url = URL(r=request,c='default',f='get_items')
    wrapper = DIV(_id=id)
    #input = SQLFORM.widgets.string.widget(field,value)
    key_id = "_autocomplete_%s_%s_aux" % (base, field.name)
    if value is not None and value != '':
        fields = [db[base][fd] for fd in content.split(',')]
        values = db[base](db[base]['id']==value)#.select(*fields)
        tmp = []
        for fd in fields:
            tmp.append(values[fd])
        value_aux = "-".join(tmp)
    else:
        value_aux = ""
    input_aux = INPUT(_name="_autocomplete_%s_%s_aux" % (base, field.name),
                      _id="%s_%s" % (field._tablename, field.name),
                      _class=field.type, _type='text',
                      _value=value_aux)
    input = INPUT(_id=key_id, _name="%s" % field.name,
                  _type='hidden', _value=value)
    script = SCRIPT("""jQuery('#%s input').autocomplete({
        source: function( request, response ) {
            $.ajax({
              url: "%s",
              dataType: "json",
              data: {
                q: request.term,
                base: '%s',
                content: '%s',
                format: '%s',
                aux_base : '%s',
              },
              success: function( data ) {
                response( $.map( data, function(item) {
                    return {
                        value: item.value,
                        uuid: item.uuid
                    }
                 }));
              }
            });
         },
        minLength: 2,
        select: function( event, ui ) {
            $("#%s").val(ui.item.uuid);
        }
      });""" % (id, callback_url, base, content, format, aux_base, key_id))
    wrapper.append(input_aux)
    wrapper.append(script)
    wrapper.append(input)
    return wrapper


def pdf_format(cabecera, footer, rows, head, foot, oficina, documento, fecha, firmas, qr_file, texto_referencia=''):
    #data = request.vars
    body = TBODY(*rows)
    table = TABLE(*[head, foot, body],
                  _border="0", _align="center", _width="100%")
#                  _border="1", _align="center", _width="100%")

    #from gluon.contrib.pyfpdf import FPDF, HTMLMixin


    class MyFPDF(FPDF, HTMLMixin):
        def header(self):
            self.set_font('Arial','B',10)
            self.cell(0,5, u"SALUDPOL", 0, 1, 'L')
            self.cell(0,5, u"GERENCIA GENERAL", 0, 1, 'L')
            self.cell(0,5, u"%s" % oficina, 0, 1, 'L')
            self.cell(0,5, u"%s" % documento, 1, 0, 'L')
            self.cell(0,5, u"Fecha: %s" % fecha, 0, 1, 'R')
            self.image('%s' % qr_file,160,247,50)
            self.ln(4)

        def footer(self):
            self.set_y(-20)
            self.set_font('Arial','I',6)
            self.cell(0,5,firmas,0,1,'L')
            self.cell(0,5,' ',0,1)

#            self.cell(0,5,firma_2,0,1,'R')

            txt = u'Página %s de %s' % (self.page_no(), self.alias_nb_pages())
            self.cell(0,5,txt,0,0,'C')

    pdf=MyFPDF()
    # first page:
    pdf.add_page()
    pdf.set_font('Arial','B',8)
    #pdf.set_line_width(0.1)
    #pdf.set_line_width(0.1)
    for line in texto_referencia:
        pdf.cell(0, 4, u"%s" % line, 0, 1, 'L')
    #pdf.set_font('Arial','B',8)
    #pdf.ln(0.4)
    cabecera = str(XML(cabecera, sanitize=False))
    cabecera = u'<font size="8">%s</font>' % cabecera.decode('utf8')
    pdf.write_html(cabecera)
    table = str(XML(table, sanitize=False))
    table = u'<font size="8">%s</font>' % table.decode('utf8')
    pdf.write_html(table)
    footer = str(XML(footer, sanitize=False))
    footer = u'<font size="8">%s</font>' % footer.decode('utf8')
    pdf.write_html(footer)
    return pdf

def generate_qrcode(name, content):
    qr = qrcode.QRCode(version=20)
    security = hashlib.sha512()
    security.update(name)
    security.update(content)
    content = u"%s\r\n%s" % (content, security.hexdigest())
    qr.add_data(content)
    qr.make()
    img = qr.make_image()
    location = '%s-%s.png' % (UPLOAD_PATH, name)
    img.save(location)
    return location

