### we prepend t_ to tablenames and f_ to fieldnames for disambiguity
db.auth_group.update_or_insert(role='root')
db.auth_group.update_or_insert(role='review')
db.auth_group.update_or_insert(role='user')
db.auth_group.update_or_insert(role='economy')
db.auth_group.update_or_insert(role='logistic')


review_id = db.auth_group(db.auth_group.role=='review')['id']

auth.settings.everybody_group_id = review_id
