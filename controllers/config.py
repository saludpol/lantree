# -*- coding: utf-8 -*-
import xlrd

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()



@auth.requires_membership('root')
def metas():
    form = SQLFORM.grid(db.metas,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def fuentes_financiamientos():
    form = SQLFORM.grid(db.fuentes_financiamientos,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def areas():
    form = SQLFORM.grid(db.areas,
        # linked_tables=[
        #     'metas',
        #     'fuentes_financiamientos',
        #     'personal',
        #     ],
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def clasificadores():
    form = SQLFORM.grid(db.clasificadores,
        # linked_tables=[
        #     'metas',
        #     'fuentes_financiamientos',
        #     'personal',
        #     ],
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def nombres_centros_costos():
    form = SQLFORM.grid(db.denominacion_centros_costos,
        # linked_tables=[
        #     'metas',
        #     'fuentes_financiamientos',
        #     'personal',
        #     ],
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def centros_costos():
    form = SQLFORM.grid(db.centros_costos,
        # linked_tables=[
        #     'metas',
        #     'fuentes_financiamientos',
        #     'personal',
        #     ],
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def genericas_gastos():
    form = SQLFORM.grid(db.genericas_gastos,
        # linked_tables=[
        #     'metas',
        #     'fuentes_financiamientos',
        #     'personal',
        #     ],
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def cadenas_funcionales():
    form = SQLFORM.grid(db.cadenas_funcionales,
        # linked_tables=[
        #     'metas',
        #     'fuentes_financiamientos',
        #     'personal',
        #     ],
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def gastos_fijos():
    form = SQLFORM.grid(db.gastos_fijos,
        # linked_tables=[
        #     'metas',
        #     'fuentes_financiamientos',
        #     'personal',
        #     ],
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def unidades_medidas():
    form = SQLFORM.grid(db.unidades_medidas,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def productos():
    form = SQLFORM.grid(db.productos,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def users_manage():
    form = SQLFORM.grid(db.auth_user,
        csv=False, deletable=False)
    return dict(form=form)


#@auth.requires(restrictions)
@auth.requires_membership('root')
def users_groups():
    form = SQLFORM.grid(db.auth_group,
        csv=False, deletable=False)
    return dict(form=form)

#@auth.requires(restrictions)
@auth.requires_membership('root')
def users_membership():
    form = SQLFORM.grid(db.auth_membership,
        csv=False, deletable=False)
    return dict(form=form)


#@auth.requires_login()
@auth.requires_membership('root')
def main_excel_process(filedata):
    filename = '%s/%s' % (UPLOAD_PATH, filedata)
    book = xlrd.open_workbook(filename, encoding_override="utf_8")
    data_col = []
    #db.affiliates.truncate()
    #db.conditions.truncate()
    # Get Sheets
    for sheet_index in range(book.nsheets):
        sheet = book.sheet_by_index(sheet_index)
        # Get Rows
        for row in range(sheet.nrows)[1:]:
            data_row = []
            # Get Cols
            for col in range(sheet.ncols):
                try:
                    valid_data = sheet.cell_type(row, col)
                    #sample_source = sheet.cell_value(row, col)
                    if valid_data == xlrd.XL_CELL_EMPTY:
                        sample_source = '0'
                    elif valid_data == xlrd.XL_CELL_TEXT:
                        sample_source = sheet.cell_value(row, col)
                    elif valid_data == xlrd.XL_CELL_NUMBER:
                        sample_source = float(sheet.cell_value(row, col))
                    elif valid_data == xlrd.XL_CELL_DATE:
                        sample_source = xlrd.xldate_as_tuple(
                            sheet.cell_value(row, col), book.datemode)
                    elif valid_data == xlrd.XL_CELL_BOOLEAN:
                        sample_source = bool(sheet.cell_value(row, col))
                    else:
                        sample_source = sheet.cell_value(row, col)
                except:
                    sample_source = '0'
                data_row.append(sample_source)
            data_col.append(data_row)
            # if data_row[0] == 'B':
            #     tipo = 0
            # else:
            #     tipo = 1
            # unidad_medida = db.unidades_medidas(db.unidades_medidas.nombre==data_row[3]) or db.unidades_medidas.insert(nombre=data_row[3])#, id=data_row[7])
            # clasificador = db.clasificadores(db.clasificadores.codigo==data_row[5]) or db.clasificadores.insert(codigo=data_row[5])
            # data_db = {'tipo': tipo,
            #            'codigo': data_row[1],
            #            'descripcion': data_row[2],
            #            'unidad_medida': unidad_medida,
            #            'costo': data_row[4],
            #            'clasificador': clasificador
            # }
            # db.productos.insert(**data_db)
    return data_col


#@auth.requires_login()
@auth.requires_membership('root')
def massive_input():
    form = SQLFORM.factory(
        Field('process_file', 'upload', label=T('Carga Masiva de Productos'),
              uploadfolder=UPLOAD_PATH),
    )
    if form.process().accepted:
        filename = form.vars.process_file
        file_type = request.vars.process_file.type
        if file_type == EXCEL_FILE or file_type == OLD_EXCEL_FILE:
            data_col = main_excel_process(filename)
            for data_row in data_col:
                if data_row[0] == 'B':
                    tipo = 0
                else:
                    tipo = 1
                unidad_medida = db.unidades_medidas(db.unidades_medidas.nombre==data_row[3]) or db.unidades_medidas.insert(nombre=data_row[3])#, id=data_row[7])
                clasificador = db.clasificadores(db.clasificadores.codigo==data_row[5]) or db.clasificadores.insert(codigo=data_row[5])
                data_db = {'tipo': tipo,
                           'codigo': data_row[1],
                           'descripcion': data_row[2],
                           'unidad_medida': unidad_medida,
                           'costo': data_row[4],
                           'clasificador': clasificador
                }
                db.productos.insert(**data_db)
            session.flash = T('Procesado')
        else:
            session.flash = T('Errors in File')
    return dict(form=form)


@auth.requires_membership('root')
def basic_massive_input():
    form = SQLFORM.factory(
        Field('process_file', 'upload', label=T('Carga Masiva de Datos Básicos'),
              uploadfolder=UPLOAD_PATH),
    )
    if form.process().accepted:
        filename = form.vars.process_file
        file_type = request.vars.process_file.type
        if file_type == EXCEL_FILE or file_type == OLD_EXCEL_FILE:
            data_col = main_excel_process(filename)
            for data_row in data_col:
                cadena = db.cadenas_funcionales(db.cadenas_funcionales.codigo==data_row[2]) or db.cadenas_funcionales.insert(codigo=data_row[2])#, id=data_row[7])
                area = db.areas(db.areas.nombre==data_row[4]) or db.areas.insert(nombre=data_row[4], responsable=data_row[6])
                data_db = {
                           'codigo': int(data_row[1]),
                           'nombre': data_row[3],
                           'cadena': cadena,
                }
                meta=db.metas.insert(**data_db)
                data_db_aux = {
                            'area' : area,
                            'nombre' : data_row[5],
                            'meta' : meta,
                            'responsable' : data_row[6]
                }
                db.centros_costos.insert(**data_db_aux)
            session.flash = T('Procesado')
        else:
            session.flash = T('Errors in File')
    return dict(form=form)