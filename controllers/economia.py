# -*- coding: utf-8 -*-
#import xlrd
import types
from numbertoletters import number_to_letters

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def devengues():
    compromiso = request.vars.compromiso
    form = SQLFORM.factory(
        Field('compromiso', label=T('Compromiso'), default=compromiso, widget=SQLFORM.widgets.autocomplete(
            request, db.compromisos.id),
            requires=IS_IN_DB(db, db.compromisos.id)
        ),
    )
    data = ''
    nombre_proveedor = ''
    tipos_comprobantes = db(db.comprobantes.id>0).select(db.comprobantes.id, db.comprobantes.nombre)
    if form.process().accepted:
        compromiso = form.vars.compromiso
        used_devengues =  db(db.devengues_detalles.id>0)._select(db.devengues_detalles.compromiso_detalle)
        data = db((db.compromisos_detalles.compromiso == compromiso) &
                  (~db.compromisos_detalles.id.belongs(used_devengues))
                ).select(
            db.compromisos_detalles.id,
            db.compromisos_detalles.certificacion_detalle,
            db.compromisos_detalles.observaciones,
            db.compromisos_detalles.ano,
            db.compromisos_detalles.mes,
            db.compromisos_detalles.monto,
            orderby=db.compromisos_detalles.mes,
        )
        proveedor = db.compromisos(db.compromisos.id==compromiso)['proveedor']
        nombre_proveedor = db.proveedores(db.proveedores.id==proveedor)['nombre']
    return dict(form=form, data=data, compromiso=compromiso, tipos_comprobantes=tipos_comprobantes,
                nombre_proveedor=nombre_proveedor)

def devengar():
    user = auth.user.id
    data = request.vars
    fecha_comprobante = data['fecha_comprobante']
    comprobante = data['comprobante']
    total = data['gran_total']
    compromiso = data['compromiso']
    tipo_comprobante = data['tipo_comprobante']
    compromisos_detalles = data['compromiso_detalle']
    if type(compromisos_detalles) is types.ListType:
        compromisos_detalles = compromisos_detalles
    else:
        compromisos_detalles = [compromisos_detalles]
    data_devengue = {
        'usuario': user,
        'compromiso': compromiso,
        'comprobante_pago': tipo_comprobante,
        'numero_comprobante': comprobante,
        'monto': total,
        'fecha_comprobante': fecha_comprobante,
        'status': 0
    }
    id = db.devengues.insert(**data_devengue)
    for compromiso_detalle in compromisos_detalles:
        tmp = {'devengue':id, 'compromiso_detalle':compromiso_detalle}
        db.devengues_detalles.insert(**tmp)
    redirect(URL('imprimir_devengue', vars=dict(devengue=id)))
    return dict()


def consultas_devengues():
    form = SQLFORM.smartgrid(db.devengues,
         linked_tables=[
             'devengues_detalles'],
         links=[dict(header=T('Imprimir'), body=lambda row: A(T('Imprimir'), _href=URL('economia','imprimir_devengue',vars=dict(devengue=row.id))))],
         deletable=False, create=False, editable=False
        )
    return dict(form=form)


def imprimir_devengue():
    devengue = request.vars.devengue
    data = ''
    if devengue is not None:
        data = db(db.devengues.id==devengue).select(
            db.devengues.ALL,
            db.devengues_detalles.ALL,
            db.compromisos.ALL,
            db.compromisos_detalles.ALL,
            db.certificaciones_detalles.ALL,
            left=(
                    (db.devengues_detalles.on(db.devengues_detalles.devengue==db.devengues.id)),
                    (db.compromisos.on(db.compromisos.id==db.devengues.compromiso)),
                    (db.compromisos_detalles.on(db.compromisos_detalles.id==db.devengues_detalles.compromiso_detalle)),
                    (db.certificaciones_detalles.on(db.certificaciones_detalles.id==db.compromisos_detalles.certificacion_detalle))
                ),
            orderby=db.certificaciones_detalles.especifica_gasto|db.compromisos_detalles.mes
            )
        oficina = u"Division de Economia - Dpto de Presupuesto"
        documento = u"Devengue: %s" % devengue
        texto_referencia = []
        firma_1 = u"Miranda Aburto Never"
        firma_2 = u"Pizarro de la Cruz Issac"
        firmas = "%s                    %s" % (firma_1, firma_2)
        i = 0
        col = i % 2 and "#F0F0F0" or "#FFFFFF"
        rows = []
        total = 0
        #sales_value = total
        for row in data:
            fecha = row.devengues.register_time.date()
            comprobante_pago = row.devengues.comprobante_pago
            numero_comprobante = row.devengues.numero_comprobante
            try:
                fecha_comprobante = row.devengues.fecha_comprobante.date()
            except:
                fecha_comprobante = ''
            compromiso = row.devengues.compromiso
            monto_devengue = float(row.devengues.monto)
            fecha_compromiso = row.compromisos.register_time.date()
            proveedor = row.compromisos.proveedor
            usuario = row.compromisos.usuario
            observaciones = row.compromisos.observaciones.decode('utf-8')
            producto = row.certificaciones_detalles.producto
            especifica_gasto = row.certificaciones_detalles.especifica_gasto
            comprobante_pago_nombre = db.comprobantes(db.comprobantes.id==comprobante_pago)['nombre']
            proveedor_nombre = db.proveedores(db.proveedores.id==proveedor)['nombre']
            producto_nombre = db.productos(db.productos.id==producto)['descripcion']
            especifica_gasto_nombre = db.clasificadores(db.clasificadores.id==especifica_gasto)['codigo']
            usuario_data = db.auth_user(db.auth_user.id==usuario)
            usuario_nombre = '%s, %s' % (usuario_data['last_name'],usuario_data['first_name'])
            rows.append(TR(
                           TD(especifica_gasto_nombre, _align="left"),
                           TD(producto_nombre, _align="left"),
                           TD(row.compromisos_detalles.ano, _align="right"),
                           TD(row.compromisos_detalles.mes, _align="right"),
                           TD("{:,.2f}".format(float(row.compromisos_detalles.monto)), _align="right"),
                           _bgcolor=col))
            total += float(row.compromisos_detalles.monto)
        print 'aaa',observaciones
        cabecera = [
                THEAD(TR(
                    TH("",_width="70%"),
                    TH("",_width="30%"))),
                TBODY(
                        TR(
                        TD(u"Compromiso: %s" % compromiso),TD(u"Fecha Compromiso: %s" % fecha_compromiso)
                        ),
                        TR(
                        TD(u"Proveedor: %s" % proveedor_nombre),TD(u"")
                        ),
                        TR(
                        TD(u"Comprobante Pago: %s" % comprobante_pago_nombre),TD(u"No.: %s" % numero_comprobante)
                        ),
                        TR(
                        TD(u"Fecha Comprobante: %s" % fecha_comprobante),TD(u"")
                        ),
                        TR(
                        TD(u"Observaciones: %s" % observaciones),TD(u"")
                        ),
                    )
                ]
        cabecera = TABLE(*cabecera, _border="0", _align="center", _width="100%")
        head = THEAD(TR(
                        TH("Especifica de Gasto",_width="30%"),
                        TH("Producto",_width="50%"),
                        TH("Año",_width="5%"),
                        TH("Mes",_width="5%"),
                        TH("Importe",_width="10%"),
                        _bgcolor="#A0A0A0"))
        foot = TFOOT(
                TR( TH(' ',_width="50%", _align="left"),
                    TH('Importe Total',_width="30%", _align="left"),
                    TH(' ',_width="5%", _align="right"),
                    TH('S/.',_width="5%", _align="right"),
                    TH("{:,.2f}".format(float(total)),_width="10%", _align="right"),
                    _bgcolor="#E0E0E0"))
        total_words = number_to_letters(int(monto_devengue))
        format_total = "{:,.2f}".format(float(monto_devengue))
        total_decimal = u"%s/100" % str(format_total).split('.')[1]
        total_letters = u"Por la cantidad de: %scon %s %s" % (total_words, total_decimal,
                                              'NUEVO SOLES')
        total_letters = total_letters.upper()
        footer = [
                THEAD(TR(
                    TH("",_width="90%"),
                    TH("",_width="10%"))),
                TBODY(
                        TR(
                        TD(u"Total Devengado: S/. %s" % monto_devengue),TD(u"")
                        ),
                        TR(
                        TD(u""),TD(u"")
                        ),
                        TR(
                        TD(u"%s" % total_letters),TD(u"")
                        ),
                    )
                ]
        footer = TABLE(*footer, _border="0", _align="center", _width="100%")
        qr_name = u"devengue-%s" % devengue
        qr_content = "SALUDPOL\r\n%s\r\n%s\r\n%s\r\n%s" % (oficina, documento, fecha, usuario_nombre)
        qr_file = generate_qrcode(qr_name, qr_content)
        pdf = pdf_format(cabecera, footer, rows, head, foot, oficina, documento, fecha, firmas, qr_file)
        response.headers['Content-Type'] = 'application/pdf'
        response.headers['Content-Disposition'] = 'attachment;filename=devengue-%s_%s.pdf' % (now.year, devengue)
        return pdf.output(dest='S')

def pagados():
    compromiso = request.vars.compromiso
    form = SQLFORM.factory(
        Field('compromiso', label=T('Compromiso'), default=compromiso, widget=SQLFORM.widgets.autocomplete(
            request, db.compromisos.id),
            requires=IS_IN_DB(db, db.compromisos.id)
        ),
    )
    data = ''
    nombre_proveedor = ''
#    tipos_comprobantes = db(db.comprobantes.id>0).select(db.comprobantes.id, db.comprobantes.nombre)
    if form.process().accepted:
        compromiso = form.vars.compromiso
        used_devengues =  db(db.pagados.id>0)._select(db.pagados.devengue)
        data = db((db.devengues.compromiso == compromiso) &
                  (~db.devengues.id.belongs(used_devengues))
                ).select(
            db.devengues.id,
            db.devengues.comprobante_pago,
            db.devengues.numero_comprobante,
            db.devengues.monto,
#            orderby=db.compromisos_detalles.mes,
        )
        proveedor = db.compromisos(db.compromisos.id==compromiso)['proveedor']
        nombre_proveedor = db.proveedores(db.proveedores.id==proveedor)['nombre']
    return dict(form=form, data=data, compromiso=compromiso, nombre_proveedor=nombre_proveedor)


def procesar_pago():
    print request.args
    devengado = request.args[0]
    compromiso = request.args[1]
    monto = float(request.args[2])
    comprobante_pago = request.args[3]
    numero_comprobante = request.args[4]
    nombre_proveedor = request.args[5]
    q = db.pagados.devengue == devengado
    db.pagados.devengue.default = devengado
    db.pagados.devengue.writable = False
    form = SQLFORM.grid(q,args=request.args,csv=False,formname = "form"+str(devengado))
    return dict(form=form)

def seleccionar_pago():
    user = auth.user.id
    devengado = request.args[0]
    compromiso = request.args[1]
    monto = float(request.args[2])
    comprobante_pago = request.args[3]
    numero_comprobante = request.args[4]
    nombre_proveedor = request.args[5]
    db.pagados.compromiso.default = compromiso
    db.pagados.compromiso.writable = False
    db.pagados.devengue.default = devengado
    db.pagados.devengue.writable = False
    db.pagados.usuario.default = user
    db.pagados.usuario.writable = False
    db.pagados.status.default = 0
    db.pagados.status.writable = False
    query_cuenta_destino = (db.proveedores_cuentas.id > 0)
    db.pagados.cuenta_destino.requires = IS_EMPTY_OR(
        IS_IN_DB(db(query_cuenta_destino), db.proveedores_cuentas.id,'%(nombre)s - %(numero)s'))
    query = db.pagados.devengue == devengado
#    datos_documento = db(db.devengues.)
    form = SQLFORM.grid(query,
            fields=[
                db.pagados.compromiso,
                db.pagados.devengue,
                db.pagados.tipo_pago,
                db.pagados.cuenta_origen,
                db.pagados.documento_generado,
                db.pagados.cuenta_destino,
                db.pagados.monto
            ],
            args=request.args,
            csv=False,formname = "form"+str(devengado)
            )
    data = db(query).select(db.pagados.monto)
    monto_total = 0
    for row in data:
        monto_total += row.monto
    saldo = monto - monto_total
    #form = SQLFORM(db.pagados)
    #form = SQLFORM.smartgrid(db.pagados)
    #print "aaaa:",compromiso, devengado
    return dict(form=form, comprobante_pago=comprobante_pago, numero_comprobante=numero_comprobante,
                monto=monto, monto_total=monto_total, saldo=saldo, nombre_proveedor=nombre_proveedor)

# def seleccionar_pago():
#     user = auth.user.id
#     data = dict(request.vars)
#     print data
#     print request.args
#     devengado=1
#     monto = 0
#     comprobante_pago = 1
#     numero_comprobante = 1
#     nombre_proveedor = False
#     if 'devengado' in data:
#         devengado = data['devengado']
#         compromiso = data['compromiso']
#         monto = float(data['monto'])
#         comprobante_pago = data['comprobante_pago']
#         numero_comprobante = data['numero_comprobante']
#         nombre_proveedor = data['nombre_proveedor']
#         db.pagados.compromiso.default = compromiso
#         db.pagados.compromiso.writable = False
#         db.pagados.devengue.default = devengado
#         db.pagados.devengue.writable = False
#     db.pagados.usuario.default = user
#     db.pagados.usuario.writable = False
#     db.pagados.status.default = 0
#     db.pagados.status.writable = False
#     query_cuenta_destino = (db.proveedores_cuentas.id > 0)
#     db.pagados.cuenta_destino.requires = IS_EMPTY_OR(
#         IS_IN_DB(db(query_cuenta_destino), db.proveedores_cuentas.id,'%(nombre)s - %(numero)s'))
#     query = (db.pagados.devengue == devengado)
# #    datos_documento = db(db.devengues.)
#     form = SQLFORM.grid(query,
#             fields=[
#                 db.pagados.compromiso,
#                 db.pagados.devengue,
#                 db.pagados.tipo_pago,
#                 db.pagados.cuenta_origen,
#                 db.pagados.documento_generado,
#                 db.pagados.cuenta_destino,
#                 db.pagados.monto
#             ],
#             args=request.args[:1]
#             )
#     data = db(query).select(db.pagados.monto)
#     monto_total = 0
#     for row in data:
#         monto_total += row.monto
#     saldo = monto - monto_total
#     #form = SQLFORM(db.pagados)
#     #form = SQLFORM.smartgrid(db.pagados)
#     #print "aaaa:",compromiso, devengado
#     return dict(form=form, comprobante_pago=comprobante_pago, numero_comprobante=numero_comprobante,
#                 monto=monto, monto_total=monto_total, saldo=saldo, nombre_proveedor=nombre_proveedor)

def consultas_pagados():
    form = SQLFORM.grid(db.pagados,
         links=[dict(header=T('Imprimir'), body=lambda row: A(T('Imprimir'), _href=URL('economia','imprimir_pagado',vars=dict(pagado=row.id))))],
         # linked_tables=[
         #     'devengues_detalles'],
         deletable=False, create=False, editable=False
        )
    return dict(form=form)


def imprimir_pagado():
    pagado = request.vars.pagado
    data = ''
    if pagado is not None:
        data = db(db.pagados.id==pagado).select(
            db.pagados.ALL,
            db.devengues.ALL,
            db.devengues_detalles.ALL,
            db.compromisos.ALL,
            db.compromisos_detalles.ALL,
            db.certificaciones_detalles.ALL,
            left=(
                    (db.devengues.on(db.devengues.id==db.pagados.devengue)),
                    (db.devengues_detalles.on(db.devengues_detalles.devengue==db.devengues.id)),
                    (db.compromisos.on(db.compromisos.id==db.pagados.compromiso)),
                    (db.compromisos_detalles.on(db.compromisos_detalles.id==db.devengues_detalles.compromiso_detalle)),
                    (db.certificaciones_detalles.on(db.certificaciones_detalles.id==db.compromisos_detalles.certificacion_detalle))
                ),
            orderby=db.certificaciones_detalles.especifica_gasto|db.compromisos_detalles.mes
            )
        oficina = u"Tesoreria"
        documento = u"Comprobante de  Pago: %s" % pagado
        texto_referencia = []
        firma_1 = u"Walter Alvarez Pino"
        firma_2 = u"Giovanna Fernandez Sanchez"
        firma_3 = u"Juana Esther Vizarreta Chia"
        firmas = "%s                    %s                    %s" % (firma_1, firma_2, firma_3)
        i = 0
        col = i % 2 and "#F0F0F0" or "#FFFFFF"
        rows = []
        total = 0
        #sales_value = total
        for row in data:
            fecha = row.devengues.register_time.date()
            comprobante_pago = row.devengues.comprobante_pago
            numero_comprobante = row.devengues.numero_comprobante
            try:
                fecha_comprobante = row.devengues.fecha_comprobante.date()
            except:
                fecha_comprobante = ''
            compromiso = row.devengues.compromiso
            monto_devengue = float(row.devengues.monto)
            fecha_compromiso = row.compromisos.register_time.date()
            proveedor = row.compromisos.proveedor
            usuario = row.compromisos.usuario
            observaciones = row.pagados.observaciones
            cuenta_origen = row.pagados.cuenta_origen
            cuenta_destino = row.pagados.cuenta_destino
            documento_generado = row.pagados.documento_generado
            tipo_pago = row.pagados.tipo_pago
            total = row.pagados.monto
            producto = row.certificaciones_detalles.producto
            especifica_gasto = row.certificaciones_detalles.especifica_gasto
            cuenta_origen_data = db.cuentas_bancarias(db.cuentas_bancarias.id==cuenta_origen)
            cuenta_origen_banco = db.bancos(db.bancos.id==cuenta_origen_data['banco'])['nombre']
            cuenta_origen_numero = cuenta_origen_data['numero']
            cuenta_destino_data = db.proveedores_cuentas(db.proveedores_cuentas.id==cuenta_destino)
            cuenta_destino_banco = db.bancos(db.bancos.id==cuenta_destino_data['banco'])['nombre']
            cuenta_destino_numero = cuenta_destino_data['numero']
            cuenta_destino_cci = cuenta_destino_data['cci']
            tipo_pago_nombre = db.tipos_pagos(db.tipos_pagos.id==tipo_pago)['nombre']
            comprobante_pago_nombre = db.comprobantes(db.comprobantes.id==comprobante_pago)['nombre']
            proveedor_nombre = db.proveedores(db.proveedores.id==proveedor)['nombre']
            producto_nombre = db.productos(db.productos.id==producto)['descripcion']
            especifica_gasto_nombre = db.clasificadores(db.clasificadores.id==especifica_gasto)['codigo']
            usuario_data = db.auth_user(db.auth_user.id==usuario)
            usuario_nombre = '%s, %s' % (usuario_data['last_name'],usuario_data['first_name'])
            #total += float(row.compromisos_detalles.monto)
        total_words = number_to_letter.to_word(int(total))
        format_total = "{:,.2f}".format(float(total))
        total_decimal = u"%s/100" % str(format_total).split('.')[1]
        total_letters = u"%scon %s %s" % (total_words, total_decimal,
                                              'NUEVO SOLES')
        total_letters = total_letters.upper()
        cabecera = [
                THEAD(TR(
                    TH("",_width="70%"),
                    TH("",_width="30%"))),
                TBODY(
                        TR(
                        TD(u"Paguese a la Orden de: %s" % proveedor_nombre),TD(u"")
                        ),
                        TR(
                        TD(u"Son: %s" % total_letters),TD(u"Total: S/.%s" % "{:,.2f}".format(float(total)))
                        ),
                        TR(
                        TD(u"Banco Origen: %s" % cuenta_origen_banco),TD(u"Cuenta: %s" % cuenta_origen_numero)
                        ),
                        TR(
                        TD(u"-------------------------------------------------" % cuenta_destino),TD(u"")
                        ),
                        TR(
                        TD(u"Banco Destino: %s" % cuenta_destino_banco),TD(u"")
                        ),
                        TR(
                        TD(u"Cuenta: %s" % cuenta_destino_numero),TD(u"CCI: %s" % cuenta_destino_cci)
                        ),
                        TR(
                        TD(u"Operacion/Cheque: %s" % documento_generado),TD(u"")
                        ),
                        TR(
                        TD(u"-------------------------------------------------" % cuenta_destino),TD(u"")
                        ),
                        TR(
                        TD(u"Comprobante Referencia: %s" % comprobante_pago_nombre),TD(u"No.: %s" % numero_comprobante)
                        ),
                        TR(
                        TD(u"Fecha Comprobante: %s" % fecha_comprobante),TD(u"")
                        ),
                    )
                ]
        cabecera = TABLE(*cabecera, _border="0", _align="center", _width="100%")
        rows.append(TR(
                       TD(tipo_pago_nombre, _align="left"),
                       TD("{:,.2f}".format(float(total)), _align="right"),
                       _bgcolor=col))
        head = THEAD(TR(
                        TH("Concepto de Pago",_width="80%"),
                        TH("Importe",_width="20%"),
                        _bgcolor="#A0A0A0"))
        foot = TFOOT(
                TR(
                    TH('Importe Total',_width="80%", _align="left"),
                    TH("S/ .{:,.2f}".format(float(total)),_width="20%", _align="right"),
                    _bgcolor="#E0E0E0"))
        footer = [
                THEAD(TR(
                    TH("",_width="90%"),
                    TH("",_width="10%"))),
                TBODY(
                        TR(
                        TD(u"compromiso: %s" % compromiso),TD(u"")
                        ),
                        TR(
                        TD(u"Observaciones: %s" % observaciones),TD(u"")
                        ),
                    )
                ]
#        footer = []
        footer = TABLE(*footer, _border="0", _align="center", _width="100%")
        qr_name = u"pagado-%s" % pagado
        qr_content = "SALUDPOL\r\n%s\r\n%s\r\n%s\r\n%s" % (oficina, documento, fecha, usuario_nombre)
        qr_file = generate_qrcode(qr_name, qr_content)
        pdf = pdf_format(cabecera, footer, rows, head, foot, oficina, documento, fecha, firmas, qr_file)
        response.headers['Content-Type'] = 'application/pdf'
        response.headers['Content-Disposition'] = 'attachment;filename=pagado-%s_%s.pdf' % (now.year, pagado)
        return pdf.output(dest='S')