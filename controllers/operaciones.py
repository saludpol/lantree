# -*- coding: utf-8 -*-
#import xlrd
import types

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def certificaciones():
    user = auth.user.id
    db.certificaciones.usuario.default = user
    db.certificaciones.usuario.writable = False
    db.certificaciones.status.default = 0
    db.certificaciones.status.writable = False
    form = SQLFORM(db.certificaciones)
#    form = SQLFORM.grid(db.certificaciones,
#        csv=False, deletable=False)
    if form.process().accepted:
        data = form.vars
        #print data
        redirect(URL('certificacion_detalle', vars=data))
    return dict(form=form)


def certificacion_detalle():
    user = auth.user.id
    certificacion = request.vars.id
    validacion = db(db.compromisos.certificacion==certificacion).select(db.compromisos.id).as_list()
    if len(validacion) == 0:
        db.certificaciones_detalles.certificacion.default = certificacion
        db.certificaciones_detalles.certificacion.writable = False
        db.certificaciones_detalles.usuario.default = user
        db.certificaciones_detalles.usuario.writable = False
        query = (db.certificaciones_detalles.certificacion == certificacion)
        form = SQLFORM.grid(query,
    #        constraints=dict(actividades=query),
            csv=False)
    else:
        form = T("Certificado Comprometido.  No es posible agregar nuevas especificas")
    return dict(form=form, certificacion=certificacion)


def consultas_certificaciones():
    form = SQLFORM.smartgrid(db.certificaciones,
         linked_tables=[
             'certificaciones_detalles'],
         links=[dict(header=T('Imprimir'), body=lambda row: A(T('Imprimir'), _href=URL('operaciones','imprimir_certificacion',vars=dict(certificacion=row.id))))],
         deletable=False, create=False, editable=False
        )
    return dict(form=form)

def imprimir_certificacion():
    certificacion = request.vars.certificacion
    data = ''
    if certificacion is not None:
        data = db(db.certificaciones.id==certificacion).select(
            db.certificaciones.ALL,
            db.certificaciones_detalles.ALL,
            left=(db.certificaciones_detalles.on(db.certificaciones_detalles.certificacion==db.certificaciones.id))
            )
        oficina = u"Oficina de Planeamiento y Presupuesto"
        documento = u"Certificacion: %s" % certificacion
        texto_referencia = [u"""
        De conformidad a lo establecido en el Art. 77 de la Ley No. 28411, Ley General del Sistema Nacional de Presupuesto,""",
                            u"""
        y en aplicación al PIM SALUDPOL AF 2014, La %s de SALUDPOL EMITE""" % (oficina),
                            u"""
                            la certificación de disponibilidad presupuestaria de acuerdo al siguiente detalle:
        """ ]
        firma_1 = u"Dra. Rebeca Perez Allpoc"
        firma_2 = u"CPC Hernando Zamora Dominguez"
        firmas = "%s                    %s" % (firma_1, firma_2)
        #data = SQLTABLE(data)
        # cabecera = [
        #         THEAD(TR(
        #             TH("Certificación No.",_width="50%"),
        #             TH(u"%s" % certificacion,_width="50%"))),
        #         TBODY(TR(
        #                 TD(u""),TD(u"")
        #                 ),
        #             )
        #         ]
        cabecera = []
        cabecera = TABLE(*cabecera, _border="0", _align="center", _width="100%")
        i = 0
        col = i % 2 and "#F0F0F0" or "#FFFFFF"
        rows = []
        total = 0
        #sales_value = total
        for row in data:
            fecha = row.certificaciones.register_time.date()
            usuario = row.certificaciones.usuario
            fuente_financiamiento = row.certificaciones.fuente_financiamiento
            especifica_gasto = row.certificaciones_detalles.especifica_gasto
            meta_presupuestal = row.certificaciones_detalles.meta_presupuestal
            tipo_proceso = row.certificaciones.tipo_proceso
            numero_proceso = row.certificaciones.numero_proceso
            observaciones = row.certificaciones.observaciones
            usuario_data = db.auth_user(db.auth_user.id==usuario)
            fuente_financiamiento_nombre = db.fuentes_financiamientos(db.fuentes_financiamientos.id==fuente_financiamiento)['nombre']
            especifica_gasto_nombre = db.clasificadores(db.clasificadores.id==especifica_gasto)['codigo']
            meta_presupuestal_nombre = db.metas(db.metas.id==meta_presupuestal)['nombre']
            tipo_proceso_nombre = db.tipos_procesos(db.tipos_procesos.id==tipo_proceso)['abreviatura']
            usuario_nombre = '%s, %s' % (usuario_data['last_name'],usuario_data['first_name'])
            tipo_proceso_texto = '%s-%s' % (tipo_proceso_nombre, numero_proceso)
            rows.append(TR(
#                           TD(fuente_financiamiento_nombre, _align="left"),
                           TD(especifica_gasto_nombre, _align="left"),
                           TD(meta_presupuestal_nombre, _align="left"),
                           TD(tipo_proceso_texto, _align="right"),
                           TD("{:,.2f}".format(float(row.certificaciones_detalles.importe)), _align="right"),
                           _bgcolor=col))
            total += float(row.certificaciones_detalles.importe)
        cabecera = [
                THEAD(TR(
                    TH("",_width="70%"),
                    TH("",_width="30%"))),
                TBODY(
                        TR(
                        TD(u"Fuente de Financiamiento: %s" % fuente_financiamiento_nombre),TD(u"")
                        ),
                        TR(
                        TD(u"Observaciones: %s" % observaciones),TD(u"")
                        ),
                    )
                ]
        cabecera = TABLE(*cabecera, _border="0", _align="center", _width="100%")
        head = THEAD(TR(
#                        TH("Fuente Financiamiento",_width="20%"),
                        TH("Especifica Gasto",_width="20%"),
                        TH("Meta Presupuestal",_width="50%"),
                        TH("Concepto",_width="20%"),
                        TH("Importe",_width="10%"),
                        _bgcolor="#A0A0A0"))
        foot = TFOOT(
                TR(
#                    TH(' ',_width="20%", _align="left"),
                    TH('Importe Total',_width="20%", _align="left"),
                    TH(' ',_width="50%", _align="right"),
                    TH('S/.',_width="20%", _align="right"),
                    TH("{:,.2f}".format(float(total)),_width="10%", _align="right"),
                    _bgcolor="#E0E0E0"))
        footer = []
        footer = TABLE(*footer, _border="0", _align="center", _width="100%")
        qr_name = u"certificacion-%s" % certificacion
        qr_content = "SALUDPOL\r\n%s\r\n%s\r\n%s\r\n%s" % (oficina, documento, fecha, usuario_nombre)
        qr_file = generate_qrcode(qr_name, qr_content)
        pdf = pdf_format(cabecera, footer, rows, head, foot, oficina, documento, fecha, firmas, qr_file, texto_referencia)
        response.headers['Content-Type'] = 'application/pdf'
        response.headers['Content-Disposition'] = 'attachment;filename=certificacion-%s_%s.pdf' % (now.year, certificacion)
        return pdf.output(dest='S')


def compromisos():
    user = auth.user.id
    db.compromisos.usuario.default = user
    db.compromisos.usuario.writable = False
    db.compromisos.status.default = 0
    db.compromisos.status.writable = False
    #validacion = db(db.compromisos.certificacion==certificacion).select(db.compromisos.id).as_list()
    form = SQLFORM(db.compromisos)
#    form = SQLFORM.grid(db.certificaciones,
#        csv=False, deletable=False)
    if form.process().accepted:
        data = form.vars
        #print data
        redirect(URL('compromiso_detalle', vars=data))
#    db.detalle_compromisos.usuario.default = user
#    db.detalle_compromisos.usuario.writable = False
#     form = SQLFORM.smartgrid(db.compromisos,
#         linked_tables=[
#             'detalle_compromisos'],
#         csv=False, deletable=False)
    return dict(form=form)


def compromiso_detalle():
    compromiso = request.vars.id
    if type(compromiso) is types.ListType:
        compromiso = compromiso[0]
    certificacion = request.vars.certificacion
    user = auth.user.id
    certificacion_detalle = {}
    validacion = db(db.devengues.compromiso==compromiso).select(db.devengues.id).as_list()
    if len(validacion) == 0:
        rows = db(db.certificaciones_detalles.certificacion==certificacion).select(db.certificaciones_detalles.id,
            db.certificaciones_detalles.especifica_gasto, db.certificaciones_detalles.meta_presupuestal,
            db.certificaciones_detalles.importe,
            orderby=db.certificaciones_detalles.id)
        for row in rows:
            certificacion_detalle[row.id] = "%s - %s - %s : S/. %s" % (row.id,
                db.clasificadores(db.clasificadores.id==row.especifica_gasto)['nombre'],
                db.metas(db.metas.id==row.meta_presupuestal)['nombre'],
                row.importe)
        db.compromisos_detalles.compromiso.default = compromiso
        db.compromisos_detalles.compromiso.writable = False
        db.compromisos_detalles.usuario.default = user
        db.compromisos_detalles.usuario.writable = False
        db.compromisos_detalles.ano.default = now.year
        db.compromisos_detalles.ano.writable = False
        db.compromisos_detalles.mes.default = now.month
        db.compromisos_detalles.certificacion_detalle.requires = IS_IN_SET(certificacion_detalle)
        query = (db.compromisos_detalles.compromiso == compromiso)
        form = SQLFORM.grid(query,
    #        constraints=dict(actividades=query),
            csv=False)
    else:
        form = T("Compromiso Cerrado.  No es posible agregar nuevos elementos")
    return dict(form=form, compromiso=compromiso)

def consultas_compromisos():
    form = SQLFORM.smartgrid(db.compromisos,
         linked_tables=[
             'compromisos_detalles'],
         links=[dict(header=T('Imprimir'), body=lambda row: A(T('Imprimir'), _href=URL('operaciones','imprimir_compromiso',vars=dict(compromiso=row.id))))],

         deletable=False, create=False, editable=False
        )
    return dict(form=form)

def imprimir_compromiso():
    compromiso = request.vars.compromiso
    data = ''
    if compromiso is not None:
        data = db(db.compromisos.id==compromiso).select(
            db.compromisos.ALL,
            db.compromisos_detalles.ALL,
            db.certificaciones_detalles.ALL,
            left=((db.compromisos_detalles.on(db.compromisos_detalles.compromiso==db.compromisos.id)),
                  (db.certificaciones_detalles.on(db.certificaciones_detalles.id==db.compromisos_detalles.certificacion_detalle))
                ),
            orderby=db.certificaciones_detalles.especifica_gasto|db.compromisos_detalles.mes
            )
        oficina = u"Oficina de Planeamiento y Presupuesto"
        documento = u"Compromiso: %s" % compromiso
        texto_referencia = []
        firma_1 = u"Dra. Rebeca Perez Allpoc"
        firma_2 = u"CPC Hernando Zamora Dominguez"
        firmas = "%s                    %s" % (firma_1, firma_2)
        i = 0
        col = i % 2 and "#F0F0F0" or "#FFFFFF"
        rows = []
        total = 0
        #sales_value = total
        for row in data:
            fecha = row.compromisos.register_time.date()
            usuario = row.compromisos.usuario
            observaciones = row.compromisos.observaciones
            producto = row.certificaciones_detalles.producto
            especifica_gasto = row.certificaciones_detalles.especifica_gasto
            producto_nombre = db.productos(db.productos.id==producto)['descripcion']
            especifica_gasto_nombre = db.clasificadores(db.clasificadores.id==especifica_gasto)['codigo']
            usuario_data = db.auth_user(db.auth_user.id==usuario)
            usuario_nombre = '%s, %s' % (usuario_data['last_name'],usuario_data['first_name'])
            rows.append(TR(
                           TD(especifica_gasto_nombre, _align="left"),
                           TD(producto_nombre, _align="left"),
                           TD(row.compromisos_detalles.ano, _align="right"),
                           TD(row.compromisos_detalles.mes, _align="right"),
                           TD("{:,.2f}".format(float(row.compromisos_detalles.monto)), _align="right"),
                           _bgcolor=col))
            total += float(row.compromisos_detalles.monto)
        cabecera = [
                THEAD(TR(
                    TH("",_width="70%"),
                    TH("",_width="30%"))),
                TBODY(
                        TR(
                        TD(u"Observaciones: %s" % observaciones),TD(u"")
                        ),
                    )
                ]
        cabecera = TABLE(*cabecera, _border="0", _align="center", _width="100%")
        head = THEAD(TR(
                        TH("Especifica Gasto",_width="30%"),
                        TH("Producto",_width="50%"),
                        TH("Año",_width="5%"),
                        TH("Mes",_width="5%"),
                        TH("Importe",_width="10%"),
                        _bgcolor="#A0A0A0"))
        foot = TFOOT(
                TR( TH(' ',_width="50%", _align="left"),
                    TH('Importe Total',_width="30%", _align="left"),
                    TH(' ',_width="5%", _align="right"),
                    TH('S/.',_width="5%", _align="right"),
                    TH("{:,.2f}".format(float(total)),_width="10%", _align="right"),
                    _bgcolor="#E0E0E0"))
        footer = []
        footer = TABLE(*footer, _border="0", _align="center", _width="100%")
        qr_name = u"compromiso-%s" % compromiso
        qr_content = "SALUDPOL\r\n%s\r\n%s\r\n%s\r\n%s" % (oficina, documento, fecha, usuario_nombre)
        qr_file = generate_qrcode(qr_name, qr_content)
        pdf = pdf_format(cabecera, footer, rows, head, foot, oficina, documento, fecha, firmas, qr_file)
        response.headers['Content-Type'] = 'application/pdf'
        response.headers['Content-Disposition'] = 'attachment;filename=compromiso-%s_%s.pdf' % (now.year, compromiso)
        return pdf.output(dest='S')
