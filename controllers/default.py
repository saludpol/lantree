# -*- coding: utf-8 -*-
import gluon.contrib.simplejson
### required - do no delete
def user():
    return dict(form=auth())

def download(): return response.download(request, db)
def call(): return service()
### end requires


def index():
    if auth.user:
        redirect(URL('home'))
    return dict(form=auth.login())


@auth.requires_login()
def home():
    return dict()


def error():
    return dict()


def get_items():
    MINCHARS = 2 # characters required to trigger response
    MAXITEMS = 20 # number of items in response
    query = request.vars.q
    base = request.vars.base
    fields = request.vars.content
    format = request.vars.format
    aux = request.vars.aux_base
    aux_table = False
    aux_field = False
    if len(aux) > 0:
        tmp_aux = aux.split(':')
        aux_origin = tmp_aux[0]
        aux_table = tmp_aux[1]
        aux_field = tmp_aux[2]
    id = db[base]['id']
    if len(query.strip()) > MINCHARS and fields and base:
        data_fields = [db[base][field] for field in format.split(',')]
        content = [db[base][field] for field in fields.split(',')]
        condition = [field.contains(query) for field in content]
        data = []
        for cond in condition:
            data.extend([ row for row in db(cond).select(id, *data_fields,
                limitby=(0, MAXITEMS)).as_list()])
        elements=[]
        for line in data:
            tmp=[]
            for field in format.split(','):
                content = line[field]
                if field == aux_origin:
                    content = db[aux_table](db[aux_table]['id']==content)[aux_field]
                tmp.append(content)
            #elements.append({'uuid':line['id'], 'value':"%s - %s" % tuple(tmp)})
            elements.append({'uuid':line['id'], 'value': "-".join(tmp)})
            #elements[line['id']] = "%s - %s" % tuple(tmp)
    else:
        elements = {}
    return gluon.contrib.simplejson.dumps(elements)