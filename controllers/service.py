# -*- coding: utf-8 -*-
#import xlrd
import unicodecsv as csv

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def get_csv():
    file_name = request.vars.file_name
    return dict(file_name=file_name)

def foda():
    area = auth.user.area
    db.areas.id.default = area
    db.areas.id.writable = False
    mode = False
    if auth.has_membership(user_id=auth.user.id, role='root'):
        mode = True
    db.areas.nombre.writable = mode
    #db.areas.meta.writable = mode
    db.areas.responsable.writable = mode
    #db.areas.fuente_financiamiento.writable = mode
    form = SQLFORM.smartgrid(db.areas,
        linked_tables=[
            'fortalezas',
            'oportunidades',
            'debilidades',
            'amenazas'
            ],
        csv=False, deletable=mode, editable=mode)
    return dict(form=form)

def personal():
    area = auth.user.area
    db.personal.area.default = area
    db.personal.area.writable = False
    form = SQLFORM.grid(db.personal,
        csv=False, deletable=False)
    return dict(form=form)

def actividades():
    area = auth.user.area
    # mode = False
    if auth.has_membership(user_id=auth.user.id, role='root'):
        query = (db.asignacion_objetivos.id > 0)
        #mode = True
    else:
        query = (db.asignacion_objetivos.area == area)
    content = {}
    data = db(query).select(
        db.asignacion_objetivos.id,
        db.asignacion_objetivos.objetivo_general,
        db.asignacion_objetivos.objetivo_especifico)
    for row in data:
        objetivo_general = db.objetivos_generales(db.objetivos_generales.id==row.objetivo_general)['objetivo']
        objetivo_especifico = db.objetivos_especificos(db.objetivos_especificos.id==row.objetivo_especifico)['objetivo']
        content[row.id] = [objetivo_general, objetivo_especifico]
    return dict(content=content)


def actividades_presupuestos():
    area = auth.user.area
    # mode = False
    if auth.has_membership(user_id=auth.user.id, role='root'):
        query = (db.asignacion_objetivos.id > 0)
        #mode = True
    else:
        query = (db.asignacion_objetivos.area == area)
    content = {}
    data = db(query).select(
        db.asignacion_objetivos.id,
        db.asignacion_objetivos.objetivo_general,
        db.asignacion_objetivos.objetivo_especifico,
        db.asignacion_objetivos.centro_costo
        )
    for row in data:
        objetivo_general = db.objetivos_generales(db.objetivos_generales.id==row.objetivo_general)['objetivo']
        objetivo_especifico = db.objetivos_especificos(db.objetivos_especificos.id==row.objetivo_especifico)['objetivo']
        try:
            centro_costo = db.centros_costos(db.centros_costos.id==row.centro_costo)['nombre']
        except:
            centro_costo = ''
        content[row.id] = [objetivo_general, objetivo_especifico, centro_costo]
    return dict(content=content)


def actividad():
    area = auth.user.area
    objetivo_referencia = request.vars.objetivo_referencia
    if auth.has_membership(user_id=auth.user.id, role='root'):
        query = ((db.actividades.id > 0) & (db.actividades.objetivo_referencia == objetivo_referencia))
        #mode = True
    else:
        query = ((db.actividades.area == area) & (db.actividades.objetivo_referencia == objetivo_referencia))
    db.actividades.area.default = area
    db.actividades.area.writable = False
    db.actividades.objetivo_referencia.default = objetivo_referencia
    db.actividades.objetivo_referencia.writable = False
    #query = (db.actividades.area == area)
    form = SQLFORM.smartgrid(db.actividades,
        fields = [
            db.actividades.area,
            db.actividades.objetivo_referencia,
            db.actividades.actividad,
            db.actividades.unidad_medida,
            db.actividades.mes_1,
            db.actividades.mes_2,
            db.actividades.mes_3,
            db.actividades.mes_4,
            db.actividades.mes_5,
            db.actividades.mes_6,
            db.actividades.mes_7,
            db.actividades.mes_8,
            db.actividades.mes_9,
            db.actividades.mes_10,
            db.actividades.mes_11,
            db.actividades.mes_12,
            db.indicadores.actividad,
            db.indicadores.denominacion,
            db.indicadores.numerador,
            db.indicadores.denominador,
            db.indicadores.linea_base,
            db.indicadores.valor_esperado,
            db.indicadores.fuente,
            db.indicadores.periodicidad,
            db.presupuestos.actividad,
            db.presupuestos.producto,
            db.presupuestos.descripcion,
            db.presupuestos.mes_1,
            db.presupuestos.mes_2,
            db.presupuestos.mes_3,
            db.presupuestos.mes_4,
            db.presupuestos.mes_5,
            db.presupuestos.mes_6,
            db.presupuestos.mes_7,
            db.presupuestos.mes_8,
            db.presupuestos.mes_9,
            db.presupuestos.mes_10,
            db.presupuestos.mes_11,
            db.presupuestos.mes_12,
            db.presupuestos.costo_unitario,
            db.presupuestos.costo_total
        ],
        linked_tables=[
            'indicadores',
            'presupuestos'],
        constraints=dict(actividades=query),
        )
    return dict(form=form)


def presupuesto():
    area = auth.user.area
    objetivo_referencia = request.vars.objetivo_referencia
    if auth.has_membership(user_id=auth.user.id, role='root'):
        query = ((db.presupuestos.id > 0) & (db.actividades.id > 0) & (db.actividades.objetivo_referencia == objetivo_referencia))
        #mode = True
    else:
        query = ((db.presupuestos.id > 0) & (db.actividades.area == area) & (db.actividades.objetivo_referencia == objetivo_referencia))
    db.actividades.area.default = area
    db.actividades.area.writable = False
    db.actividades.objetivo_referencia.default = objetivo_referencia
    db.actividades.objetivo_referencia.writable = False
    sum = db.presupuestos.costo_total.sum()
    data_sum = db(query).select(db.presupuestos.actividad,sum,
                                left=(
                                    db.actividades.on(db.actividades.id==db.presupuestos.actividad)
                                ),
                                groupby=db.presupuestos.actividad)
    #actividades = {}
    monto_total = 0
    for row in data_sum:
        monto_total += row._extra['SUM(presupuestos.costo_total)']
        #actividades[row.presupuestos['actividad']] = row._extra['SUM(presupuestos.costo_total)']
    #query = (db.actividades.area == area)
    form = SQLFORM.grid(query,
#                        db.presupuestos.ALL,
        fields = [
#            db.presupuestos.ALL,
            # db.actividades.area,
            # db.actividades.objetivo_referencia,
            # db.actividades.actividad,
            # db.actividades.unidad_medida,
            # db.actividades.mes_1,
            # db.actividades.mes_2,
            # db.actividades.mes_3,
            # db.actividades.mes_4,
            # db.actividades.mes_5,
            # db.actividades.mes_6,
            # db.actividades.mes_7,
            # db.actividades.mes_8,
            # db.actividades.mes_9,
            # db.actividades.mes_10,
            # db.actividades.mes_11,
            # db.actividades.mes_12,
            # db.indicadores.actividad,
            # db.indicadores.denominacion,
            # db.indicadores.numerador,
            # db.indicadores.denominador,
            # db.indicadores.linea_base,
            # db.indicadores.valor_esperado,
            # db.indicadores.fuente,
            # db.indicadores.periodicidad,
            db.metas.codigo,
            db.presupuestos.actividad,
            db.productos.descripcion,
            db.unidades_medidas.nombre,
            db.clasificadores.codigo,
            db.presupuestos.descripcion,
            db.presupuestos.mes_1,
            db.presupuestos.mes_2,
            db.presupuestos.mes_3,
            db.presupuestos.mes_4,
            db.presupuestos.mes_5,
            db.presupuestos.mes_6,
            db.presupuestos.mes_7,
            db.presupuestos.mes_8,
            db.presupuestos.mes_9,
            db.presupuestos.mes_10,
            db.presupuestos.mes_11,
            db.presupuestos.mes_12,
            db.presupuestos.costo_unitario,
            db.presupuestos.costo_total
        ],
        left=(
                db.actividades.on(db.actividades.id==db.presupuestos.actividad),
                db.productos.on(db.productos.id==db.presupuestos.producto),
                db.unidades_medidas.on(db.unidades_medidas.id==db.productos.unidad_medida),
                db.clasificadores.on(db.clasificadores.id==db.productos.clasificador),
                db.centros_costos.on(db.centros_costos.id==db.actividades.centro_costo),
                db.metas.on(db.metas.id==db.centros_costos.meta)
            )
        # linked_tables=[
        #     'actividades'],
        # constraints=dict(actividades=query),
        )
    return dict(form=form, monto_total=monto_total)

def reporte_especifico():
    area = auth.user.area
    centro_costo = request.vars.centro_costo
    try:
        nombre_area = db.areas(db.areas.id==area)['nombre']
    except:
        nombre_area = ''
    data = ''
    actividades = ''
    lines = []
    csv_path = False
    form = SQLFORM.factory(
        Field('centro_costo', db.centros_costos, label=T('Centro de Costo'), default=centro_costo,
            #widget=SQLFORM.widgets.autocomplete(request, db.centros_costos.nombre),
            #requires=IS_IN_DB(db, 'centros_costos.id', 'centros_costos.nombre')
            requires=IS_EMPTY_OR(IS_IN_DB(db, db.centros_costos, '%(nombre)s'))
        ),
    )
    if form.process().accepted:
        centro_costo = form.vars.centro_costo
        if centro_costo is not None:
            query = ((db.actividades.area == area) & (db.actividades.centro_costo == centro_costo))
        else:
            query = (db.actividades.area == area)
        if auth.has_membership(user_id=auth.user.id, role='root'):
            if centro_costo is not None:
                query = ((db.actividades.id > 0) & (db.actividades.centro_costo == centro_costo))
            else:
                query = (db.actividades.id > 0)
        sum = db.presupuestos.costo_total.sum()
        data_sum = db(db.presupuestos.id>0).select(db.presupuestos.actividad,sum, groupby=db.presupuestos.actividad)
        actividades = {}
        for row in data_sum:
            actividades[row.presupuestos['actividad']] = row._extra['SUM(presupuestos.costo_total)']
        data = db(query).select(
            db.actividades.ALL,
            db.asignacion_objetivos.objetivo_general,
            db.asignacion_objetivos.objetivo_especifico,
            db.centros_costos.ALL,
            left=(
                db.asignacion_objetivos.on(db.actividades.objetivo_referencia==db.asignacion_objetivos.id),
                (
                    db.centros_costos.on(db.actividades.centro_costo==db.centros_costos.id)
                )
            ),
            orderby=db.asignacion_objetivos.area|db.actividades.centro_costo
        )
        suma_total = 0
        lines = []
        for row in data:
            tmp = []
            objetivo_general = row.asignacion_objetivos.objetivo_general
            objetivo_especifico = row.asignacion_objetivos.objetivo_especifico
            meta_presupuestal = row.centros_costos.meta
            actividad = row.actividades.id
            unidad_medida = row.actividades.unidad_medida
            meta_fisica = row.actividades.meta
            centro_costo = row.actividades.centro_costo
            area = row.actividades.area
            data = []
            for val in range(1,13):
                tmp_month = 'mes_%s' % val
                try:
                    val = int(row.actividades[tmp_month])
                except:
                    val = 0
                pass
                data.append(val)
            pass
            primer_trimestre = data[0] + data[1] + data[2]
            segundo_trimestre = data[3] + data[4] + data[5]
            tercer_trimestre = data[6] + data[7] + data[8]
            cuarto_trimestre = data[9] + data[10] + data[11]
            try:
                nombre_centro_costo=db.centros_costos(db.centros_costos.id==centro_costo)['nombre']
            except:
                nombre_centro_costo=''
            pass
            tmp.append(nombre_centro_costo)
            try:
                objetivo_nombre=db.objetivos_generales(db.objetivos_generales.id==objetivo_general)['objetivo']
            except:
                objetivo_nombre=''
            pass
            tmp.append(objetivo_nombre)
            try:
                objetivo_nombre=db.objetivos_especificos(db.objetivos_especificos.id==objetivo_especifico)['objetivo']
            except:
                objetivo_nombre=''
            pass
            tmp.append(objetivo_nombre)
            try:
                meta_nombre=db.metas(db.metas.id==meta_presupuestal)['codigo']
            except:
                meta_nombre=''
            pass
            tmp.append(meta_nombre)
            try:
                actividad_nombre=db.actividades(db.actividades.id==actividad)['actividad']
            except:
                actividad_nombre=''
            pass
            tmp.append(actividad_nombre)
            try:
                unidad_medida_nombre=db.unidades_medidas(db.unidades_medidas.id==unidad_medida)['nombre']
            except:
                unidad_medida_nombre=''
            pass
            tmp.append(unidad_medida_nombre)
            tmp.append(meta_fisica)
            tmp.append(primer_trimestre)
            tmp.append(segundo_trimestre)
            tmp.append(tercer_trimestre)
            tmp.append(cuarto_trimestre)
            if actividad in actividades:
                suma_actividad = actividades[actividad]
            else:
                suma_actividad = 0
            tmp.append("{:,.2f}".format(suma_actividad))
            try:
                suma_total += suma_actividad
            except:
                suma_total += 0
            pass
            lines.append(tmp)
        last_line = ['','','','','','','','','','','',"{:,.2f}".format(suma_total)]
        lines.append(last_line)
        headers = ['Centro de Costo','Objetivo General','Objetivo Especifico', 'Meta Presupuestal',
                   'Actividad', 'Unidad Medida', 'Meta Fisica', 'Primer Trimestre', 'Segundo Trimestre',
                   'Tercer Trimestre', 'Cuarto Trimestre', 'Presupuesto'
                   ]
        file_name = "reporte_especifico-%s.csv" % (now)
        file_url = "%s/%s" % (UPLOAD_PATH, file_name)
        csv_path = "%s" % file_name
        #content = data.as_list()
        out = csv.writer(open(file_url,"w"), delimiter=',',quoting=csv.QUOTE_ALL)
        out.writerow(headers)
        out.writerows(lines)
    return dict(form=form, lines=lines, nombre_area=nombre_area, csv_path=csv_path)


def reporte_especificas_gastos():
    area = auth.user.area
    centro_costo = request.vars.centro_costo
    try:
        nombre_area = db.areas(db.areas.id==area)['nombre']
    except:
        nombre_area = ''
    lines = ''
    especificas = ''
    new_lines = ''
    csv_path = False
    form = SQLFORM.factory(
        Field('centro_costo', db.centros_costos, label=T('Centro de Costo'), default=centro_costo,
            #widget=SQLFORM.widgets.autocomplete(request, db.centros_costos.nombre),
            #requires=IS_IN_DB(db, 'centros_costos.id', 'centros_costos.nombre')
            requires=IS_EMPTY_OR(IS_IN_DB(db, db.centros_costos, '%(nombre)s'))
        ),
    )
    if form.process().accepted:
        centro_costo = form.vars.centro_costo
        if centro_costo is not None:
            query = ((db.actividades.area == area) & (db.actividades.centro_costo == centro_costo))
        else:
            query = (db.actividades.area == area)
        if auth.has_membership(user_id=auth.user.id, role='root'):
            if centro_costo is not None:
                query = ((db.actividades.id > 0) & (db.actividades.centro_costo == centro_costo))
            else:
                query = (db.actividades.id > 0)
        sum = db.presupuestos.costo_total.sum()
        data_sum = db((db.presupuestos.id>0) & query).select(
                        db.productos.clasificador,sum,
                        left=(
                            (db.productos.on(db.productos.id==db.presupuestos.producto)),
                            (db.actividades.on(db.actividades.id==db.presupuestos.actividad))
                        ),
                        groupby=db.productos.clasificador)
        especificas = {}
        for row in data_sum:
            if row.productos['clasificador'] is not None:
                especificas[row.productos['clasificador']] = row._extra['SUM(presupuestos.costo_total)']
        data = db(query).select(
#            db.especificas.centro_costo,
            db.productos.clasificador,
            db.productos.tipo,
            db.presupuestos.ALL,
#            db.especificas.centro_costo,
            left=(
                (db.presupuestos.on(db.presupuestos.actividad==db.actividades.id)),
                (db.productos.on(db.productos.id==db.presupuestos.producto)),
            ),
#            groupby=db.productos.clasificador,
            orderby=db.productos.clasificador
        )
        contenido = []
        for row in data:
            especifica = row.productos.clasificador
            costo_total = row.presupuestos.costo_total
            costo_unitario = row.presupuestos.costo_unitario
            tipo = row.productos.tipo
            cantidad = 0
            for num in range(1, 13):
                #meses.setdefault(num, row.presupuestos['mes_%s' % num])
                try:
                    cantidad += row.presupuestos['mes_%s' % num]
                except:
                    cantidad += 0
            meses = {}
            for num in range(1, 13):
                if tipo == 0:
                    meses[num] = row.presupuestos['mes_%s' % num] * costo_unitario
                else:
                    try:
                        meses[num] = (row.presupuestos['mes_%s' % num] * costo_total) / cantidad
                    except:
                        meses[num] = 0
            tmp = [especifica, costo_total, meses]
            contenido.append(tmp)
        lines = {}
        new_lines = []
        suma_total=0
        total_mes={}
        for num in range(1,13):
            total_mes[num] = 0
        for row in contenido:
            clasificador = row[0]
            elem = db.clasificadores(db.clasificadores.id==clasificador)
            try:
                codigo = elem['codigo']
            except:
                codigo = ''
            if clasificador is None:
                continue
            try:
                nombre = elem['nombre']
            except:
                nombre = ''
            lines.setdefault(codigo,{'nombre':nombre, 'total':especificas[clasificador]})
            #lines[row[0]]['total'] = row[1]
            for num in range(1, 13):
                nombre = 'mes_%s' % num
                valor = row[2][num]
                if nombre in lines[codigo]:
                    lines[codigo][nombre] += valor
                else:
                    lines[codigo][nombre] = valor
                lines[codigo][nombre]
                total_mes[num] += valor
        gastos_fijos = db((db.gastos_fijos.id>0) & (db.centros_costos.denominacion==centro_costo)).select(
                        db.productos.clasificador,db.gastos_fijos.id,db.gastos_fijos.centro_costo,
                        db.gastos_fijos.producto,db.gastos_fijos.ALL,
                        left=(
 #                           (db.actividades.on(db.actividades.id==db.presupuestos.actividad)),
                            (db.centros_costos.on(db.centros_costos.id==db.gastos_fijos.centro_costo)),
                            (db.productos.on(db.productos.id==db.gastos_fijos.producto)),
                        ),
                        )
        for row in gastos_fijos:
            clasificador = row.productos.clasificador
            elem = db.clasificadores(db.clasificadores.id==clasificador)
            try:
                codigo = elem['codigo']
            except:
                codigo = ''
            if clasificador is None:
                continue
            try:
                nombre = elem['nombre']
            except:
                nombre = ''
            neo_data = {
                'nombre':nombre,
                'total': row.gastos_fijos.monto,
                'mes_1': row.gastos_fijos.mes_1,
                'mes_2': row.gastos_fijos.mes_2,
                'mes_3': row.gastos_fijos.mes_3,
                'mes_4': row.gastos_fijos.mes_4,
                'mes_5': row.gastos_fijos.mes_5,
                'mes_6': row.gastos_fijos.mes_6,
                'mes_7': row.gastos_fijos.mes_7,
                'mes_8': row.gastos_fijos.mes_8,
                'mes_9': row.gastos_fijos.mes_9,
                'mes_10': row.gastos_fijos.mes_10,
                'mes_11': row.gastos_fijos.mes_11,
                'mes_12': row.gastos_fijos.mes_12,
            }
            lines.setdefault(codigo,neo_data)
            for num in range(1, 13):
                mes = 'mes_%s' % num
                valor = lines[codigo][mes]
                lines[codigo][mes] += valor
        for especifica in lines:
            new_lines.append([especifica, lines[especifica]['nombre'], "{:,.2f}".format(lines[especifica]['total']),
                              "{:,.2f}".format(lines[especifica]['mes_1']), "{:,.2f}".format(lines[especifica]['mes_2']),
                              "{:,.2f}".format(lines[especifica]['mes_3']), "{:,.2f}".format(lines[especifica]['mes_4']),
                              "{:,.2f}".format(lines[especifica]['mes_5']), "{:,.2f}".format(lines[especifica]['mes_6']),
                              "{:,.2f}".format(lines[especifica]['mes_7']), "{:,.2f}".format(lines[especifica]['mes_8']),
                              "{:,.2f}".format(lines[especifica]['mes_9']), "{:,.2f}".format(lines[especifica]['mes_10']),
                              "{:,.2f}".format(lines[especifica]['mes_11']), "{:,.2f}".format(lines[especifica]['mes_12'])
                              ])
            suma_total += lines[especifica]['total']
        tmp = ['', '']
        tmp.append("{:,.2f}".format(suma_total))
        for num in range(1, 13):
            tmp.append("{:,.2f}".format(total_mes[num]))
        new_lines.append(tmp)
 #        sum = db.gastos_fijos.monto.sum()
 #        gastos_fijos = db((db.gastos_fijos.id>0) & (db.centros_costos.denominacion==centro_costo)).select(
 #                        db.productos.clasificador,sum,
 #                        left=(
 # #                           (db.actividades.on(db.actividades.id==db.presupuestos.actividad)),
 #                            (db.centros_costos.on(db.centros_costos.id==db.gastos_fijos.centro_costo)),
 #                            (db.productos.on(db.productos.id==db.gastos_fijos.producto)),
 #                        ),
 #                        groupby=db.gastos_fijos.clasificador)
        headers = ['Especifica Gasto','Nombre','Total', 'Enero',
                   'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio',
                   'Julio', 'Agosto', 'Setiembre','Octubre',
                   'Noviembre', 'Diciembre'
                   ]
        file_name = "reporte_especificas-%s.csv" % (now)
        file_url = "%s/%s" % (UPLOAD_PATH, file_name)
        csv_path = "%s" % file_name
        #content = data.as_list()
        out = csv.writer(open(file_url,"w"), delimiter=',',quoting=csv.QUOTE_ALL)
        out.writerow(headers)
        out.writerows(new_lines)
    return dict(form=form, data=new_lines, nombre_area=nombre_area, csv_path=csv_path)

def presupuestos():
#    area = auth.user.area
#    db.personal.area.default = area
#    db.personal.area.writable = False
    form = SQLFORM.grid(db.presupuestos)
    return dict(form=form)

def gastos_fijos():
#    area = auth.user.area
#    db.personal.area.default = area
#    db.personal.area.writable = False
    form = SQLFORM.grid(db.gastos_fijos)
    return dict(form=form)

def reporte_diferencial():
    area = auth.user.area
    centro_costo = request.vars.centro_costo
    form = SQLFORM.factory(
        Field('centro_costo', db.denominacion_centros_costos, label=T('Centro de Costo'), default=centro_costo,
            requires=IS_EMPTY_OR(IS_IN_DB(db, db.denominacion_centros_costos, '%(nombre)s')))

    )
    data = []
    fijos = []
    total_actividades = 0
    total_gastos_fijos = 0
    techo_programado = 0
    if form.process().accepted:
        centro_costo = form.vars.centro_costo
        data_centro_costo = db(db.centros_costos.denominacion==centro_costo).select(db.centros_costos.presupuesto)
        techo_programado = 0
        for row in data_centro_costo:
            if row.presupuesto is not None:
                techo_programado += row.presupuesto
        techo_programado = round(techo_programado, 2)
        sum = db.presupuestos.costo_total.sum()
        actividades = db((db.presupuestos.id>0) & (db.centros_costos.denominacion==centro_costo)).select(
                        db.actividades.actividad,db.actividades.id,db.centros_costos.meta,sum,
                        left=(
                            (db.actividades.on(db.actividades.id==db.presupuestos.actividad)),
                            (db.centros_costos.on(db.centros_costos.id==db.actividades.centro_costo)),
                        ),
                        groupby=db.actividades.actividad|db.actividades.id|db.centros_costos.meta)
        data = []
        total_actividades = 0
        for row in actividades:
            meta = db.metas(db.metas.id==row['centros_costos']['meta'])['codigo']
            monto = row._extra['SUM(presupuestos.costo_total)']
            tmp = [row['actividades']['id'], row['actividades']['actividad'], meta, monto]
            total_actividades += monto
            data.append(tmp)
#        print data
        sum = db.gastos_fijos.monto.sum()
        gastos_fijos = db((db.gastos_fijos.id>0) & (db.centros_costos.denominacion==centro_costo)).select(
                        db.gastos_fijos.centro_costo,db.productos.clasificador,sum,
                        left=(
 #                           (db.actividades.on(db.actividades.id==db.presupuestos.actividad)),
                            (db.centros_costos.on(db.centros_costos.id==db.gastos_fijos.centro_costo)),
                            (db.productos.on(db.productos.id==db.gastos_fijos.producto)),
                        ),
                        groupby=db.gastos_fijos.id|db.productos.clasificador)
#        data = []
#        print gastos_fijos
        total_gastos_fijos = 0
        fijos = []
        for row in gastos_fijos:
            centro_costo_data = db.centros_costos(db.centros_costos.id==row['gastos_fijos']['centro_costo'])
            meta_presupuestal = db.metas(db.metas.id==centro_costo_data['meta'])['codigo']
            clasificador_data = db.clasificadores(db.clasificadores.id==row['productos']['clasificador'])
            codigo_clasificador = clasificador_data['codigo']
            nombre_clasificador = clasificador_data['nombre']
            monto = row._extra['SUM(gastos_fijos.monto)']
            tmp = [codigo_clasificador,nombre_clasificador,meta_presupuestal,monto]
            total_gastos_fijos += monto
            fijos.append(tmp)
    return dict(form=form, data=data, fijos=fijos,
                total_actividades=total_actividades, total_gastos_fijos=total_gastos_fijos,
                techo_programado=techo_programado)


def editar_actividad():
    area = auth.user.area
    actividad = request.vars.actividad
    db.actividades.area.default = area
    db.actividades.area.writable = False
    query = (db.actividades.id == actividad)
    form = SQLFORM.smartgrid(db.actividades,
        fields = [
            db.actividades.area,
            db.actividades.objetivo_referencia,
            db.actividades.actividad,
            db.actividades.unidad_medida,
            db.actividades.mes_1,
            db.actividades.mes_2,
            db.actividades.mes_3,
            db.actividades.mes_4,
            db.actividades.mes_5,
            db.actividades.mes_6,
            db.actividades.mes_7,
            db.actividades.mes_8,
            db.actividades.mes_9,
            db.actividades.mes_10,
            db.actividades.mes_11,
            db.actividades.mes_12,
            db.indicadores.actividad,
            db.indicadores.denominacion,
            db.indicadores.numerador,
            db.indicadores.denominador,
            db.indicadores.linea_base,
            db.indicadores.valor_esperado,
            db.indicadores.fuente,
            db.indicadores.periodicidad,
            db.presupuestos.actividad,
            db.presupuestos.producto,
            db.presupuestos.descripcion,
            db.presupuestos.mes_1,
            db.presupuestos.mes_2,
            db.presupuestos.mes_3,
            db.presupuestos.mes_4,
            db.presupuestos.mes_5,
            db.presupuestos.mes_6,
            db.presupuestos.mes_7,
            db.presupuestos.mes_8,
            db.presupuestos.mes_9,
            db.presupuestos.mes_10,
            db.presupuestos.mes_11,
            db.presupuestos.mes_12,
            db.presupuestos.costo_unitario,
            db.presupuestos.costo_total
        ],
        linked_tables=[
            'indicadores',
            'presupuestos'],
        constraints=dict(actividades=query),
        )
    return dict(form=form)

def procesar_actividad():
#    area = auth.user.area
    form = SQLFORM.smartgrid(db.actividades,
        fields = [
            db.actividades.area,
            db.actividades.objetivo_referencia,
            db.actividades.actividad,
            db.actividades.unidad_medida,
            db.actividades.mes_1,
            db.actividades.mes_2,
            db.actividades.mes_3,
            db.actividades.mes_4,
            db.actividades.mes_5,
            db.actividades.mes_6,
            db.actividades.mes_7,
            db.actividades.mes_8,
            db.actividades.mes_9,
            db.actividades.mes_10,
            db.actividades.mes_11,
            db.actividades.mes_12,
            db.indicadores.actividad,
            db.indicadores.denominacion,
            db.indicadores.numerador,
            db.indicadores.denominador,
            db.indicadores.linea_base,
            db.indicadores.valor_esperado,
            db.indicadores.fuente,
            db.indicadores.periodicidad,
            db.presupuestos.actividad,
            db.presupuestos.producto,
            db.presupuestos.descripcion,
            db.presupuestos.mes_1,
            db.presupuestos.mes_2,
            db.presupuestos.mes_3,
            db.presupuestos.mes_4,
            db.presupuestos.mes_5,
            db.presupuestos.mes_6,
            db.presupuestos.mes_7,
            db.presupuestos.mes_8,
            db.presupuestos.mes_9,
            db.presupuestos.mes_10,
            db.presupuestos.mes_11,
            db.presupuestos.mes_12,
            db.presupuestos.costo_unitario,
            db.presupuestos.costo_total
        ],
        linked_tables=[
            'indicadores',
            'presupuestos'],
#        constraints=dict(actividades=query),
        )
    return dict(form=form)