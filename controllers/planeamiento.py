# -*- coding: utf-8 -*-
#import xlrd

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

@auth.requires_membership('root')
def perfil():
    form = SQLFORM.smartgrid(db.planes,
        linked_tables=[
            'objetivos_estrategicos'],
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def objetivos():
    user = auth.user.id
    db.discusiones_generales.usuario.default = user
    db.discusiones_generales.usuario.writable = False
    db.discusiones_especificos.usuario.default = user
    db.discusiones_especificos.usuario.writable = False
    form = SQLFORM.smartgrid(db.planes,
        linked_tables=[
            'objetivos_generales',
            'objetivos_especificos',
            'discusiones_generales',
            'discusiones_especificos'
            ],
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def asignacion_objetivos():
    mode = True
    # if auth.has_membership(user_id=auth.user.id, role='root'):
    #     mode = True
    form = SQLFORM.grid(db.asignacion_objetivos,
        csv=False, deletable=mode, editable=mode)
    return dict(form=form)

# @auth.requires_membership('root')
# @auth.requires_membership('root')
# def asignacion_objetivos():
#     form = SQLFORM.grid(db.asignacion_objetivos,
#         csv=False, deletable=False, editable=False, create=False)
#     return dict(form=form)