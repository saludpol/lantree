# -*- coding: utf-8 -*-
import unicodecsv as csv

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

def get_csv():
    file_name = request.vars.file_name
    return dict(file_name=file_name)

@auth.requires_membership('root')
def matriz_01():
    query = (db.asignacion_objetivos.id > 0)
    data = db(query).select(
#        db.asignacion_objetivos.id,
        db.asignacion_objetivos.objetivo_general,
        db.asignacion_objetivos.objetivo_especifico,
        db.asignacion_objetivos.centro_costo,
        orderby=db.asignacion_objetivos.objetivo_general
        )
    lines = []
    for row in data:
        objetivo_general = db.objetivos_generales(db.objetivos_generales.id==row.objetivo_general)['objetivo']
        objetivo_especifico = db.objetivos_especificos(db.objetivos_especificos.id==row.objetivo_especifico)['objetivo']
        try:
            centro_costo = db.centros_costos(db.centros_costos.id==row.centro_costo)['nombre']
        except:
            centro_costo = ''
        lines.append([objetivo_general, objetivo_especifico, centro_costo])

    headers = ['Objetivo General', 'Objetivo Especifico', 'Centro de Costo']
    footers = []
    return dict(data=lines, headers=headers, footers=footers)

@auth.requires_membership('root')
def matriz_02():
    query = (db.asignacion_objetivos.id > 0)
    sum = db.presupuestos.costo_total.sum()
    data_sum = db(db.presupuestos.id > 0).select(db.presupuestos.actividad, sum, groupby=db.presupuestos.actividad)

    actividades = {}
    for row in data_sum:
        actividades[row.presupuestos['actividad']] = row._extra['SUM(presupuestos.costo_total)']

    sum_gastos_fijos = db.gastos_fijos.monto.sum()
    gastos_fijos = db(db.gastos_fijos.id > 0).select(db.gastos_fijos.actividad, sum_gastos_fijos, groupby=db.gastos_fijos.actividad)

    for row in gastos_fijos:
        if not actividades.has_key(row.gastos_fijos['actividad']):
            actividades[row.gastos_fijos['actividad']] = row._extra['SUM(gastos_fijos.monto)']
        else:
            actividades[row.gastos_fijos['actividad']] += row._extra['SUM(gastos_fijos.monto)']

    test_monto = 0.0
    for monto in actividades.values():
        test_monto += monto

    count = db.indicadores.id.count()
    data = db(query).select(
        db.asignacion_objetivos.objetivo_general,
        db.asignacion_objetivos.objetivo_especifico,
        db.actividades.id,
        db.actividades.area,
        db.actividades.actividad,
        # db.indicadores.denominacion,
        # db.indicadores.numerador,
        # db.indicadores.denominador,
        # db.indicadores.valor_esperado,
        # db.indicadores.fuente,
        # db.indicadores.periodicidad,
        db.actividades.centro_costo,
        count,
        left=(
            db.actividades.on(db.actividades.objetivo_referencia==db.asignacion_objetivos.id),
            db.indicadores.on(db.indicadores.actividad==db.actividades.id)
        ),
        groupby=(db.asignacion_objetivos.id, db.actividades.id)
        )

    monto_total = 0.0
    lines = []
    for row in data:
        objetivo_general = db.objetivos_generales(db.objetivos_generales.id==row.asignacion_objetivos.objetivo_general)['objetivo']
        objetivo_especifico = db.objetivos_especificos(db.objetivos_especificos.id==row.asignacion_objetivos.objetivo_especifico)['objetivo']
        actividad = row.actividades.actividad

        try:
            presupuesto = actividades[row.actividades.id]
        except:
            presupuesto = 0

        try:
            area = db.areas(db.areas.id==row.actividades.area)['nombre']
        except:
            area = ''
        try:
            centro_costo = db.centros_costos(db.centros_costos.id==row.actividades.centro_costo)['nombre']
        except:
            centro_costo = ''

        cant_indicadores = row._extra['COUNT(indicadores.id)']
        if cant_indicadores == 0:
            tmp = [objetivo_general.upper(), objetivo_especifico.upper(),
                   actividad.upper(), '',  '', '', '', '', '', '',
                   "{:,.2f}".format(presupuesto), '',
                   "{:,.2f}".format(presupuesto), area.upper(),
                   centro_costo.upper()]
            lines.append(tmp)
        else:
            indicadores = db(db.indicadores.actividad==row.actividades.id).select()

            for indicador in indicadores:
                denominacion = indicador.denominacion if indicador.denominacion else ''
                numerador = indicador.numerador if indicador.numerador else ''
                denominador = indicador.denominador if indicador.denominador else ''
                valor_esperado = indicador.valor_esperado if indicador.valor_esperado else ''
                fuente = indicador.fuente if indicador.fuente else ''
                periodicidad = indicador.periodicidad if indicador.periodicidad else ''

                tmp = [objetivo_general.upper(), objetivo_especifico.upper(),
                       actividad.upper(), denominacion.upper(),
                       numerador.upper(), denominador.upper(),
                       valor_esperado.upper(), fuente.upper(),
                       periodicidad.upper(), '',
                       "{:,.2f}".format(presupuesto/cant_indicadores),
                       '', "{:,.2f}".format(presupuesto/cant_indicadores),
                       area.upper(), centro_costo.upper()]
                lines.append(tmp)

        monto_total += presupuesto

    headers = ['Objetivo General','Objetivo Especifico', 'Actividad','Denominacion',
               'Numerador', 'Denominador', 'Valor Esperado', 'Fuente', 'Periodicidad',
               'RDR', 'DYT', 'Otros','Presupuesto Asignado S/.', 'Área','Centro Costo']
    footers = ['', '', '', '', '', '', '', '', '', '', "{:,.2f}".format(monto_total), '', "{:,.2f}".format(monto_total), '', '']
    file_name = "matriz_02-%s.csv" % (now)
    file_url = "%s/%s" % (UPLOAD_PATH, file_name)
    csv_path = "%s" % file_name
    out = csv.writer(open(file_url, "w"), delimiter=',', quoting=csv.QUOTE_ALL)
    out.writerow(headers)
    out.writerows(lines)
    out.writerow(footers)
    return dict(data=lines, headers=headers, footers=footers, csv_path=csv_path)

@auth.requires_membership('root')
def matriz_03():
    query = (db.asignacion_objetivos.id > 0)
    sum = db.presupuestos.costo_total.sum()
    data_sum = db(db.presupuestos.id>0).select(db.presupuestos.actividad,sum, groupby=db.presupuestos.actividad)
    actividades = {}
    orden_actividades = []
    for row in data_sum:
        actividades[row.presupuestos['actividad']] = row._extra['SUM(presupuestos.costo_total)']
    data = db(query).select(
#        db.asignacion_objetivos.id,
        db.asignacion_objetivos.objetivo_general,
        db.asignacion_objetivos.objetivo_especifico,
        db.actividades.id,
        db.actividades.area,
        db.actividades.centro_costo,
        db.actividades.actividad,
        db.actividades.unidad_medida,
        db.actividades.meta,
        db.actividades.mes_1,
        db.actividades.mes_2,
        db.actividades.mes_3,
        db.actividades.mes_4,
        db.actividades.mes_5,
        db.actividades.mes_6,
        db.actividades.mes_7,
        db.actividades.mes_8,
        db.actividades.mes_9,
        db.actividades.mes_10,
        db.actividades.mes_11,
        db.actividades.mes_12,
        db.presupuestos.producto,
        db.productos.clasificador,
        db.presupuestos.costo_total,
        left=(
            db.actividades.on(db.actividades.objetivo_referencia==db.asignacion_objetivos.id),
            db.presupuestos.on(db.presupuestos.actividad==db.actividades.id),
            db.productos.on(db.productos.id==db.presupuestos.producto)
        ),
        orderby=db.asignacion_objetivos.objetivo_general|db.asignacion_objetivos.objetivo_especifico|db.actividades.area|db.actividades.centro_costo|db.actividades.id
        )
    new_data = {}
    csv_path = False
    for row in data:
        orden_actividades.append(row.actividades.id)
        objetivo_general = db.objetivos_generales(db.objetivos_generales.id==row.asignacion_objetivos.objetivo_general)['objetivo']
        objetivo_especifico = db.objetivos_especificos(db.objetivos_especificos.id==row.asignacion_objetivos.objetivo_especifico)['objetivo']
        try:
            unidad_medida = db.unidades_medidas(db.unidades_medidas.id==row.actividades.unidad_medida)['nombre']
        except:
            unidad_medida = 'nd'
        try:
            clasificador = db.clasificadores(db.clasificadores.id==row.productos.clasificador)['codigo']
            raiz_clasificador = clasificador.split('.')[0]
            cadena_cladificador = 'generica_%s' % raiz_clasificador
        except:
            cadena_cladificador = 'nd'
        try:
            area = db.areas(db.areas.id==row.actividades.area)['nombre']
        except:
            area = ''
        try:
            centro_costo = db.centros_costos(db.centros_costos.id==row.asignacion_objetivos.centro_costo)['nombre']
        except:
            centro_costo = ''
        try:
            presupuesto = actividades[row.actividades.id]
        except:
            presupuesto = 0
        try:
            costo = row.presupuestos.costo_total + 0
        except:
            costo = 0
        tmp_data = {
            # 'codigo_objetivo_general': row.asignacion_objetivos.objetivo_general,
            # 'codigo_objetivo_especifico': row.asignacion_objetivos.objetivo_especifico,
            # 'codigo_area': row.actividades.area,
            'objetivo_general':objetivo_general,
            'objetivo_especifico':objetivo_especifico,
            'actividad':row.actividades.actividad,
            'unidad_medida':unidad_medida,
            'meta_fisica': row.actividades.meta,
            'area': area,
            'centro_costo': centro_costo,
            'dyt':presupuesto,
            'presupuesto':presupuesto,
            'primer_trimestre': 0,
            'segundo_trimestre': 0,
            'tercer_trimestre': 0,
            'cuarto_trimestre': 0,
            'generica_21': 0,
            'generica_22': 0,
            'generica_23': 0,
            'generica_24': 0,
            'generica_25': 0,
            'generica_26': 0,
            'nd':0
        }
        new_data.setdefault(row.actividades.id,tmp_data)
        primer_trimestre = 0
        segundo_trimestre = 0
        tercer_trimestre = 0
        cuarto_trimestre = 0
        for num in range(1, 13):
            nombre = 'mes_%s' % num
            valor = row.actividades[nombre]
            try:
                valor += 0
            except:
                valor = 0
            if num < 4:
                primer_trimestre += valor
            elif num >= 4 and num <=6:
                segundo_trimestre += valor
            elif num >= 7 and num <=9:
                tercer_trimestre += valor
            elif num > 9 :
                cuarto_trimestre += valor
        new_data[row.actividades.id]['primer_trimestre'] = primer_trimestre
        new_data[row.actividades.id]['segundo_trimestre'] = segundo_trimestre
        new_data[row.actividades.id]['tercer_trimestre'] = tercer_trimestre
        new_data[row.actividades.id]['cuarto_trimestre'] = cuarto_trimestre
        try:
            new_data[row.actividades.id][cadena_cladificador] += costo
        except:
            new_data[row.actividades.id][cadena_cladificador] = costo
    gastos_fijos = db((db.gastos_fijos.id>0)).select(
                    db.productos.clasificador,db.productos.unidad_medida,db.gastos_fijos.id,db.gastos_fijos.centro_costo,
                    db.gastos_fijos.producto,db.gastos_fijos.ALL,
                    left=(
#                           (db.actividades.on(db.actividades.id==db.presupuestos.actividad)),
#                        (db.centros_costos.on(db.centros_costos.id==db.gastos_fijos.centro_costo)),
                        (db.productos.on(db.productos.id==db.gastos_fijos.producto)),
                    ),
                    orderby=db.gastos_fijos.centro_costo
                    )
    for row in gastos_fijos:
        gastos_fijos_id = "GF%s" % row.gastos_fijos.id
        clasificador = row.productos.clasificador
        objetivo_referencia = row.gastos_fijos.objetivo_referencia
        presupuesto = row.gastos_fijos.monto
        unidad_medida = row.productos.unidad_medida
        data_objetivo = db.asignacion_objetivos(db.asignacion_objetivos.id==objetivo_referencia)
        data_clasificador = db.clasificadores(db.clasificadores.id==clasificador)
        data_unidad_medida = db.unidades_medidas(db.unidades_medidas.id==unidad_medida)
        if clasificador is None:
            continue
        # try:
        #     codigo = data_clasificador['codigo']
        # except:
        #     codigo = ''
        # try:
        #     nombre = data_clasificador['nombre']
        # except:
        #     nombre = ''
        try:
            objetivo_general = data_objetivo['objetivo_general']
        except:
            objetivo_general = ''
        try:
            objetivo_especifico = data_objetivo['objetivo_especifico']
        except:
            objetivo_especifico = ''
        try:
            unidad_medida = data_unidad_medida['nombre']
        except:
            unidad_medida = ''
        try:
            centro_costo = db.centros_costos(db.centros_costos.id==row.gastos_fijos.centro_costo)['nombre']
        except:
            centro_costo = ''
        try:
            clasificador = db.clasificadores(db.clasificadores.id==row.productos.clasificador)['codigo']
            raiz_clasificador = clasificador.split('.')[0]
            cadena_cladificador = 'generica_%s' % raiz_clasificador
        except:
            cadena_cladificador = 'nd'
        objetivo_general = db.objetivos_generales(db.objetivos_generales.id==objetivo_general)['objetivo']
        objetivo_especifico = db.objetivos_especificos(db.objetivos_especificos.id==objetivo_especifico)['objetivo']
        tmp_data = {
            'objetivo_general':objetivo_general,
            'objetivo_especifico':objetivo_especifico,
            'actividad':'',
            'unidad_medida':unidad_medida,
            'meta_fisica': row.gastos_fijos.cantidad,
            'area': '',
            'centro_costo': centro_costo,
            'dyt':presupuesto,
            'presupuesto':presupuesto,
            'primer_trimestre': 0,
            'segundo_trimestre': 0,
            'tercer_trimestre': 0,
            'cuarto_trimestre': 0,
            'generica_21': 0,
            'generica_22': 0,
            'generica_23': 0,
            'generica_24': 0,
            'generica_25': 0,
            'generica_26': 0,
            'nd': 0
        }
        new_data.setdefault(gastos_fijos_id, tmp_data)
        primer_trimestre = 0
        segundo_trimestre = 0
        tercer_trimestre = 0
        cuarto_trimestre = 0
        for num in range(1, 13):
            nombre = 'mes_%s' % num
            valor = row.gastos_fijos[nombre]
            try:
                valor += 0
            except:
                valor = 0
            if num < 4:
                primer_trimestre += valor
            elif num >= 4 and num <=6:
                segundo_trimestre += valor
            elif num >= 7 and num <=9:
                tercer_trimestre += valor
            elif num > 9 :
                cuarto_trimestre += valor
        new_data[gastos_fijos_id]['primer_trimestre'] = primer_trimestre
        new_data[gastos_fijos_id]['segundo_trimestre'] = segundo_trimestre
        new_data[gastos_fijos_id]['tercer_trimestre'] = tercer_trimestre
        new_data[gastos_fijos_id]['cuarto_trimestre'] = cuarto_trimestre
        try:
            new_data[gastos_fijos_id][cadena_cladificador] += costo
        except:
            new_data[gastos_fijos_id][cadena_cladificador] = costo
        orden_actividades.append(gastos_fijos_id)

    lines = []
    unique = []
    [unique.append(item) for item in orden_actividades if item not in unique]

    monto_total = 0.0
    for actividad in unique:
        content = new_data[actividad]
        lines.append(
            [
                content['objetivo_general'],
                content['objetivo_especifico'],
                content['actividad'],
                content['unidad_medida'],
                content['meta_fisica'],
                content['primer_trimestre'],
                content['segundo_trimestre'],
                content['tercer_trimestre'],
                content['cuarto_trimestre'],
                '',
                content['dyt'],
                '',
                content['generica_21'],
                content['generica_22'],
                content['generica_23'],
                content['generica_24'],
                content['generica_25'],
                content['generica_26'],
                content['presupuesto'],
                '',
                content['area'],
                content['centro_costo']
            ]
        )
        monto_total += content['presupuesto']

    headers = ['Objetivo General','Objetivo Especifico', 'Actividad',
               'Unidad Medida', 'Meta Fisica', 'Primer Trimestre', 'Segundo Trimestre', 'Tercer Trimestre',
               'Cuarto Trimestre', 'RDR', 'DYT','OTROS', 'Personal y Obligaciones (21)','Pensiones y Otras Prestaciones (22)',
               'Bienes y Servicios (23)','Donaciones y Transferencias (24)', 'Otros Gastos (25)', 'Activos No Financiaeros (26',
               'Presupuesto Total', 'Meta Presupuestal', 'Area', 'Centro Costo'
               ]
    footers = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '',
               '', '', "{:,.2f}".format(monto_total), '', '', '']
    file_name = "matriz_03-%s.csv" % (now)
    file_url = "%s/%s" % (UPLOAD_PATH, file_name)
    csv_path = "%s" % file_name
    #content = data.as_list()
    out = csv.writer(open(file_url,"w"), delimiter=',',quoting=csv.QUOTE_ALL)
    out.writerow(headers)
    out.writerows(lines)
    out.writerow(footers)
    return dict(data=lines, headers=headers, footers=footers, csv_path=csv_path)


def control_errores():
    data_1 = db(db.actividades.id>0).select(
        db.actividades.ALL,
        db.presupuestos.ALL,
        left=(
            db.presupuestos.on(db.presupuestos.actividad==db.actividades.id),
        )
        )
    errores_centros = []
    errores_especificas = []
    errores_objetivos = []
    for row in data_1:
        id = row.actividades.id
        data_centro_costo = row.actividades.centro_costo
        data_producto = row.presupuestos.producto
        data_objetivo = row.actividades.objetivo_referencia
        try:
            centro_costo = db.centros_costos(db.centros_costos.id==data_centro_costo)['nombre']
        except:
            centro_costo = ''
            errores_centros.append(id)
        try:
            objetivo = db.asignacion_objetivos(db.asignacion_objetivos.id==data_objetivo)['objetivo_especifico']
        except:
            objetivo = ''
            errores_objetivos.append(id)
        try:
            especifica = db.productos(db.productos.id==data_producto)['codigo']
        except:
            errores_especificas.append(id)
    return dict(errores_centros=errores_centros, errores_especificas=errores_especificas, errores_objetivos=errores_objetivos)


def metas_presupuestales():
    sum = db.presupuestos.costo_total.sum()
    actividades = db(db.presupuestos.id > 0).select(
                    db.centros_costos.nombre, db.centros_costos.meta, db.centros_costos.presupuesto, sum,
                    left=(
                        (db.actividades.on(db.actividades.id == db.presupuestos.actividad)),
                        (db.centros_costos.on(db.centros_costos.id == db.actividades.centro_costo)),
                    ),
                    groupby=db.centros_costos.nombre | db.centros_costos.meta | db.centros_costos.presupuesto,
                    orderby=db.centros_costos.nombre | db.centros_costos.meta | db.centros_costos.presupuesto,
                    )

    sum_alt = db.gastos_fijos.monto.sum()
    gastos_fijos = db(db.gastos_fijos.id > 0).select(
                    db.gastos_fijos.centro_costo, sum_alt,
                    groupby=db.gastos_fijos.centro_costo,
                    orderby=db.gastos_fijos.centro_costo
    )

    extra_lines = {}
    for row in gastos_fijos:
        centro_costo = row.gastos_fijos.centro_costo
        data_centro_costo = db.centros_costos(db.centros_costos.id==centro_costo)
        try:
            meta_presupuestal = db.metas(db.metas.id==data_centro_costo['meta'])['codigo']
        except:
            meta_presupuestal = ''
        try:
            nombre = '%s - %s' % (data_centro_costo['nombre'], meta_presupuestal)
        except:
            nombre = ''
        try:
            monto = float(row._extra['SUM(gastos_fijos.monto)'])
        except:
            monto = 0.0
        extra_lines[nombre] = [meta_presupuestal, monto]

    lines = []
    total_pia = 0.0
    total_presupuesto = 0.0
    total_saldo = 0.0
    for row in actividades:
        tmp = []
        nombre = row.centros_costos.nombre
        if nombre is None:
            nombre = ''
        tmp.append(nombre)
        try:
            meta_presupuestal = db.metas(db.metas.id==row.centros_costos.meta)['codigo']
        except:
            meta_presupuestal = ''
        tmp.append(meta_presupuestal)
        try:
            presupuesto = float(row.centros_costos.presupuesto)
        except:
            presupuesto = 0.0
        tmp.append("{:,.2f}".format(presupuesto))
        try:
            monto = float(row._extra['SUM(presupuestos.costo_total)'])
        except:
            monto = 0
        extra_nombre = '%s - %s' % (nombre, meta_presupuestal)
        if extra_nombre in extra_lines:
            if meta_presupuestal == extra_lines[extra_nombre][0]:
                monto += float(extra_lines[extra_nombre][1])
        tmp.append("{:,.2f}".format(monto))
        diferencia = presupuesto - monto
        tmp.append("{:,.2f}".format(diferencia))
        lines.append(tmp)
        total_pia += presupuesto
        total_presupuesto += monto
        total_saldo += diferencia

    lines.append(['', '', "{:,.2f}".format(total_pia), "{:,.2f}".format(total_presupuesto), "{:,.2f}".format(total_saldo)])
    return dict(actividades=lines)


def metas_actividades_clasificadores():
    sum = db.presupuestos.costo_total.sum()
    data = db(db.presupuestos.id > 0).select(
        db.centros_costos.meta,
        db.actividades.id,
        db.actividades.actividad,
        db.productos.clasificador, sum,
        left=(
            (db.actividades.on(db.actividades.id==db.presupuestos.actividad)),
            (db.centros_costos.on(db.centros_costos.id==db.actividades.centro_costo)),
            (db.productos.on(db.productos.id==db.presupuestos.producto))
        ),
        groupby=db.centros_costos.meta | db.actividades.id | db.actividades.actividad | db.productos.clasificador,
        orderby=db.centros_costos.meta | db.actividades.actividad | db.productos.clasificador,
        )

    sum_alt = db.gastos_fijos.monto.sum()
    gastos_fijos = db(db.gastos_fijos.id > 0).select(
        db.centros_costos.meta,
        db.actividades.id,
        db.actividades.actividad,
        db.productos.clasificador, sum_alt,
        left=(
            (db.actividades.on(db.actividades.id == db.gastos_fijos.actividad)),
            (db.centros_costos.on(db.centros_costos.id == db.gastos_fijos.centro_costo)),
            (db.productos.on(db.productos.id == db.gastos_fijos.producto))
        ),
        groupby=db.centros_costos.meta | db.actividades.id | db.actividades.actividad | db.productos.clasificador,
        orderby=db.centros_costos.meta | db.actividades.actividad | db.productos.clasificador,
    )

    metas_gastos_fijos = {}
    for row in gastos_fijos:
        try:
            codigo_meta = db.metas(db.metas.id == row.centros_costos.meta)['codigo']
        except:
            codigo_meta = ''

        id_actividad = row.actividades.id
        actividad = row.actividades.actividad
        clasificador = row.productos.clasificador

        if clasificador:
            codigo = db.clasificadores(db.clasificadores.id==clasificador)['codigo']
        else:
            codigo = ''

        try:
            monto = float(row._extra['SUM(gastos_fijos.monto)'])
        except:
            monto = 0.0

        key_actividad = str(id_actividad) + '|' + actividad + '|' + str(codigo)
        if codigo_meta in metas_gastos_fijos.keys():
            if key_actividad in metas_gastos_fijos[codigo_meta].keys():
                metas_gastos_fijos[codigo_meta][key_actividad] += monto
            else:
                metas_gastos_fijos[codigo_meta][key_actividad] = monto
        else:
            metas_gastos_fijos[codigo_meta] = {key_actividad:  monto}

    metas_presupuestos = {}
    actividades = []
    for row in data:
        try:
            codigo_meta = db.metas(db.metas.id == row.centros_costos.meta)['codigo']
        except:
            codigo_meta = ''

        id_actividad = row.actividades.id
        actividad = row.actividades.actividad
        clasificador = row.productos.clasificador
        if clasificador:
            codigo = db.clasificadores(db.clasificadores.id==clasificador)['codigo']
        else:
            codigo = ''

        try:
            monto = float(row._extra['SUM(presupuestos.costo_total)'])
        except:
            monto = 0.0

        key_actividad = str(id_actividad) + '|' + actividad + '|' + str(codigo)

        if codigo_meta in metas_presupuestos.keys():
            if key_actividad in metas_presupuestos[codigo_meta].keys():
                metas_presupuestos[codigo_meta][key_actividad] += monto
            else:
                metas_presupuestos[codigo_meta][key_actividad] = monto
        else:
            metas_presupuestos[codigo_meta] = {key_actividad:  monto}

    for codigo_meta in metas_gastos_fijos:
        actividades_gatos_fijos = metas_gastos_fijos[codigo_meta]
        if codigo_meta in metas_presupuestos.keys():
            for key_actividad in actividades_gatos_fijos:
                actividad_gastos_fijos = actividades_gatos_fijos[key_actividad]
                if key_actividad in metas_presupuestos[codigo_meta].keys():
                    metas_presupuestos[codigo_meta][key_actividad] += actividad_gastos_fijos
                else:
                    metas_presupuestos[codigo_meta][key_actividad] = actividad_gastos_fijos
        else:
            metas_presupuestos[codigo_meta] = actividades_gatos_fijos

    monto_total = 0.0
    for codigo_meta in metas_presupuestos:
        for actividad, monto in metas_presupuestos[codigo_meta].items():
            _, _nombre_actividad, _codigo = actividad.split('|')
            actividades.append([codigo_meta, _nombre_actividad, _codigo,
                                "{:,.2f}".format(monto)])
            monto_total += monto

    actividades.append(['', '', '', "{:,.2f}".format(monto_total)])
    headers = ['Meta', 'Actividad', 'Clasificador', 'Monto S/.']
    file_name = "actividades-%s.csv" % now
    file_url = "%s/%s" % (UPLOAD_PATH, file_name)
    csv_path = "%s" % file_name
    out = csv.writer(open(file_url, "w"), delimiter=',', quoting=csv.QUOTE_ALL)
    out.writerow(headers)
    out.writerows(actividades)
    return dict(actividades=actividades, headers=headers, csv_path=csv_path)