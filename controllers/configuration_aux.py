# -*- coding: utf-8 -*-
import xlrd

### required - do no delete
def user(): return dict(form=auth())
def download(): return response.download(request,db)
def call(): return service()
### end requires
def index():
    return dict()

def error():
    return dict()

@auth.requires_membership('root')
def tipos_pagos():
    form = SQLFORM.grid(db.tipos_pagos,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def tipos_procesos():
    form = SQLFORM.grid(db.tipos_procesos,
        csv=False, deletable=False)
    return dict(form=form)


@auth.requires_membership('root')
def comprobantes():
    form = SQLFORM.grid(db.comprobantes,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def monedas():
    form = SQLFORM.grid(db.monedas,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def bancos():
    form = SQLFORM.grid(db.bancos,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def tipos_cuentas():
    form = SQLFORM.grid(db.bancos_tipos_cuentas,
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def cuentas_bancarias():
    form = SQLFORM.smartgrid(db.cuentas_bancarias,
        linked_tables=[
            'cuentas_saldos'],
        csv=False, deletable=False)
    return dict(form=form)

@auth.requires_membership('root')
def proveedores():
    form = SQLFORM.smartgrid(db.proveedores,
        linked_tables=[
            'proveedores_direcciones',
            'proveedores_contactos',
            'proveedores_contactos_telefonos',
            'proveedores_cuentas'
            ],
        csv=False, deletable=False)
    return dict(form=form)